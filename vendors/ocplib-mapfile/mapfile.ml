(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

type t (* a mapped file *)

external openfile :
  filename: string ->
  readonly: bool ->
  mapsize: int64 ->
  create: bool ->
  t
  = "ocp_mapfile_openfile_c"

let openfile ?mapsize ?readonly ?create filename =
  let readonly = match readonly with
    | Some readonly -> readonly | None -> false
  in
  let create = match create with
    | Some create -> create | None -> not readonly
  in
  let mapsize = match mapsize with
    | Some mapsize -> mapsize | None -> 400_000_000_000L
  in
  openfile ~mapsize ~readonly ~create ~filename

(* write a bytes array at a given position, and return the next position *)
external write_string :
  t -> off:int -> string -> pos:int -> len:int -> int
  = "ocp_mapfile_write_string_c"

(* write a bytes array at a given position, without header, and return
   the next position *)
external write_fixed_string :
  t -> off:int -> string -> pos:int -> len:int -> int
  = "ocp_mapfile_write_fixed_string_c"

(* write a char array at a given position, and return the next position *)
external write_bytes : t -> off:int -> bytes -> pos:int -> len:int -> int
  = "ocp_mapfile_write_string_c"

(* write a char array at a given position, without header, and return
   the next position *)
external write_fixed_bytes : t -> off:int -> bytes -> pos:int -> len:int -> int
  = "ocp_mapfile_write_fixed_string_c"

(* write a char array at a given position, and return the next position *)
external write :
  t -> off:int -> Bigstring.t -> pos:int -> len:int -> int
  = "ocp_mapfile_write_bigstring_c"

(* write a char array at a given position, without header, and return
   the next position *)
external write_fixed :
  t -> off:int -> Bigstring.t -> pos:int -> len:int -> int
  = "ocp_mapfile_write_fixed_bigstring_c"

(* read from a given position, returning a Bigstring.t containing the
   content (that was written with `write_bytes`) *)
external read : t -> off:int -> Bigstring.t
  = "ocp_mapfile_read_bigstring_c"

external read_fixed : t -> off:int -> len:int -> Bigstring.t
  = "ocp_mapfile_read_fixed_bigstring_c"

external read_size : t -> off:int -> int
  = "ocp_mapfile_read_size_c"

external read_next : t -> off:int -> int
  = "ocp_mapfile_read_next_c"

external commit : t -> sync:bool -> unit
  = "ocp_mapfile_commit_c"

external close : t -> unit
  = "ocp_mapfile_close_c"

(* the returned size might be smaller than the file for read-only access *)
external size : t -> int
  = "ocp_mapfile_size_c"

external write_int : t -> off:int -> int -> int
  = "ocp_mapfile_write_int_c"

external read_int : t -> off:int -> int
  = "ocp_mapfile_read_size_c"

(* Truncate to a smaller size *)
external truncate : t -> int -> unit
  = "ocp_mapfile_truncate_c"

(* Warning: you must never use write_fixed on a file if you plan to use
   this function. *)
let iter t ?(off=0) ~max_off f =
  let rec iter t ~off ~max_off f =
    if off < max_off then
      let s = read t ~off in
      f ~off s;
      let off = read_next t ~off in
      iter t ~off ~max_off f
  in
  iter t ~off ~max_off f

(*

let random_string n =
  String.init n (fun _ -> char_of_int (Random.int 256) )


let test () =

  let list = ref [] in
  begin
    let t = openfile
        ~map_size:40_000_000_000L
        ~filename:"test-mapfile.db"
        ~create:true
        ~read_only:false
    in

    let pos = ref 0 in
    for len = 0 to 10000 do

      let s = random_string len in
      let pos1 = !pos in
      let pos2 = write_fixed_string t ~off:pos1 s ~pos:0 ~len in
      let pos3 = write_string t ~off:pos2 s ~pos:0 ~len in
      pos := pos3;
      list := (s, pos1, pos2, len) :: !list
    done;

    List.iter (fun (s,pos1, pos2, len) ->

        let b = read_fixed t ~off:pos1 ~len:len in
        assert (Bigstring.to_string b = s);

        let b = read t ~off:pos2 in
        assert (Bigstring.to_string b = s);
      ) !list;

    commit t ~sync:true;
    close t;
  end;

  begin
    let t = openfile
        ~map_size:40_000_000_000L
        ~filename:"test-mapfile.db"
        ~read_only:true
        ~create:false
    in
    List.iter (fun (s,pos1, pos2, len) ->

        let b = read_fixed t ~off:pos1 ~len:len in
        assert (Bigstring.to_string b = s);

        let b = read t ~off:pos2 in
        assert (Bigstring.to_string b = s);

        let sz = read_size t ~off:pos2 in
        assert (sz = len);
      ) !list;
    close t;
  end;


  exit 14
*)
