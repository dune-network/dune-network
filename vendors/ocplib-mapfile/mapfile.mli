(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

(* We ensure that write_bytes/write_string ALWAYS introduces at least
   a gap of 8 bytes. The cost on Tezos storage is less than 1%. *)

type t (* a mapped file *)

val openfile :
  ?mapsize:int64 -> ?readonly:bool -> ?create:bool -> string -> t

(* write a bytes array at a given position, and return the next position *)
external write_string :
  t -> off:int -> string -> pos:int -> len:int -> int
  = "ocp_mapfile_write_string_c"

(* write a bytes array at a given position, without header, and return
   the next position *)
external write_fixed_string :
  t -> off:int -> string -> pos:int -> len:int -> int
  = "ocp_mapfile_write_fixed_string_c"

(* write a char array at a given position, and return the next position *)
external write_bytes : t -> off:int -> bytes -> pos:int -> len:int -> int
  = "ocp_mapfile_write_string_c"

(* write a char array at a given position, without header, and return
   the next position *)
external write_fixed_bytes : t -> off:int -> bytes -> pos:int -> len:int -> int
  = "ocp_mapfile_write_fixed_string_c"

(* write a char array at a given position, and return the next position *)
external write :
  t -> off:int -> Bigstring.t -> pos:int -> len:int -> int
  = "ocp_mapfile_write_bigstring_c"

(* write a char array at a given position, without header, and return
   the next position *)
external write_fixed :
  t -> off:int -> Bigstring.t -> pos:int -> len:int -> int
  = "ocp_mapfile_write_fixed_bigstring_c"

(* read from a given position, returning a Bigstring.t containing the
   content (that was written with `write_bytes`) *)
external read : t -> off:int -> Bigstring.t
  = "ocp_mapfile_read_bigstring_c"

external read_fixed : t -> off:int -> len:int -> Bigstring.t
  = "ocp_mapfile_read_fixed_bigstring_c"

external read_size : t -> off:int -> int
  = "ocp_mapfile_read_size_c"

external read_next : t -> off:int -> int
  = "ocp_mapfile_read_next_c"

external commit : t -> sync:bool -> unit
  = "ocp_mapfile_commit_c"

external close : t -> unit
  = "ocp_mapfile_close_c"

(* the returned size might be smaller than the file for read-only access *)
external size : t -> int
  = "ocp_mapfile_size_c"

external write_int : t -> off:int -> int -> int
  = "ocp_mapfile_write_int_c"

external read_int : t -> off:int -> int
  = "ocp_mapfile_read_size_c"

(* Warning: you must never use write_fixed on a file if you plan to use
   this function. *)
val iter :
  t -> ?off:int -> max_off:int -> (off:int -> Bigstring.t -> unit) -> unit

(* Truncate to a smaller size *)
external truncate : t -> int -> unit
  = "ocp_mapfile_truncate_c"
