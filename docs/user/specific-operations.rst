Dune Specific Operations
========================

Dune Network provides three additionnal operations:
* ``Manage_account`` to manage the parameters of an account
* ``Activate_protocol`` to change the protocol or its constants
* ``Manage_accounts`` to manage multiple accounts

The first operation can be used by any account. The two other
operations can only be used by a :ref:`superadmin <superadmin>`
account.

.. _Manage_account:

The ``Manage_account`` operation
--------------------------------

The ``Manage_account`` operation is used to change the parameters of a
particular account, or to perform an action on behalf of that account.
If the account has an administrator (``admin``), the administrator
must sign the operation, unless it is a smart contract.

JSON Format
~~~~~~~~~~~

The content of the operation is a JSON file containing some of the
following fields (missing fields are interpreted as unchanged):

* ``maxrolls`` is either an integer or ``null``, and specifies the
  maximal number of rolls that a baker wants to have;
* ``admin`` is either another contract or ``null``, and specifies the contract
  that should be used to administrate the account. Once administrated,
  the ``admin`` key must be used to send the ``Manage_account`` operation.
* ``white_list`` is a list of contracts that are the only ones that can
  receive transfers. An empty list means that transfers are allowed to all
  accounts.
* ``delegation`` is a boolean, and specifies whether new delegations
  to this account are allowed (``true``, the default) or forbidden
  (``false``).
* ``recovery`` is either another contract or ``null``, and specifies a
  contract that can be used to recover all the tokens from this account.
  The recovery account is usually kept in cold storage, and only used
  if the key of the account is lost.
* ``actions`` is a list of pairs, containing the name of the action and
  an argument.

Available Actions
~~~~~~~~~~~~~~~~~

The following actions are currently available:

* ``clear_delegations``: this action takes an empty argument, and
  clear all the accounts delegated to this contract, which must be a
  baker. This action can be used by a baker stopping its services.
* ``move_delegations``: this action takes another baker account, and
  transfers all delegations to the current account to the other baker
  account. This action can be used by a baker changing its baking
  key.
* ``recover_account``: this actions takes another account, whose
  recovery account is the current account. The full balance of the
  other account is transferred to the current account, and the account
  is deleted.

Example of JSON file
~~~~~~~~~~~~~~~~~~~~

The following JSON file gives an example of values for all the fields
managed by ``Manage_account``::

  {
    "maxrolls": 100,
    "admin": "dn1XXX...",
    "white_list": [ "dn1XXX..." , "dn1XXX..." ],
    "delegation": false,
    "recovery": "dn1XXX...",
    "actions": [
       [ "clear_delegations", null ],
       [ "move_delegations", { "addr": "dn1XXX..." } ],
       [ "recover_account", { "addr": "dn1XXX..." } ]
    ]
  }

Inject with ``dune-client``
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following command can be used to inject a ``Manage_account`` operation::

  dune-client dune change account parameters JSONFILE of ACCOUNT with admin ACCOUNT

The following command can be used to get the current values (except the
``actions`` field)::

  dune-client dune get account parameters for ACCOUNT
  
The section on Baker Account Management contains other commands that
can be used to set most of the fields, one by one.

.. _Activate_protocol:

The ``Activate_protocol`` operation
-----------------------------------

The ``Activate_protocol`` operation is used to activate a new
protocol, or change the constants of the protocol. It can also be used
to trigger specific protocol actions. All of these changes are
scheduled to happen at a given level, at the end of the block
construction. This operation can only be used by a :ref:`superadmin`
account.

Operation Contents
~~~~~~~~~~~~~~~~~~

The operation contains:

* The ``level`` at which the change should happen (at the end of the
  block construction);
* The optional hash of a new protocol
* An optional set of protocol parameters

The protocol parameters (specified in a JSON file) can contain:

* New values for the protocol constants
* An optional ``protocol_revision`` to trigger a change of protocol
  revision. Even protocol revisions are reserved for stable upgrades,
  while odd protocol revisions are used for testing
  purposes. Typically, new features are available in the ``next``
  branch with the latest stable protocol revision + 1.
* A list of ``protocol_actions``

Currently available protocol actions are:

* The ``freeze_undelegated_accounts`` action can be used to iter on
  all contracts and freeze undelegated accounts for at least 60
  cycles;
* The ``burn_all_commitments`` action can be used to remove all
  commitments (blinded key hashes with corresponding duns, which are
  burnt during the operation);
* The ``activate_burn_legacy`` action can be used to start burning
  undelegated burnable accounts;
* The ``deactivate_burn_legacy`` action can be used to stop burning
  undelegated burnable accounts;

Inject with ``dune_client``
~~~~~~~~~~~~~~~~~~~~~~~~~~~

To inject the ``Activate_protocol`` operation, the following command
can be used::
  
  dune-client dune activate protocol PROTOCOL_HASH with key SUPERADMIN and parameters JSONFILE at level LEVEL

To only change the parameters, the following command can be used::
  
  dune-client dune change parameters JSONFILE with key SUPERADMIN at level LEVEL

If a protocol activation has been scheduled, the following command can
be used to get the upcoming change::

  dune-client dune get activate protocol

The following command can be used to print the current protocol
constants::

  dune-client dune output protocol parameters

.. _Manage_accounts:

The ``Manage_accounts`` operation
---------------------------------

The ``Manage_accounts`` operation is used to perform actions on
accounts. The operation must be sent by a superadmin account.

The operation can contain the following fields:

* ``commitments``: a list of pairs of a blinded key hash and an
  amount. The amount is taken from the account of the superadmin
  account. If a commitment already exists, the difference is debited
  or credited from the superadmin account;
* ``allow_peer_ids`` and ``revoke_peer_ids`` are two lists of peer
  identifiers used with :ref:`On-Chain Access
  Control<Privacy_onchain_acl>`;
* ``fixes``: a list of paths and corresponding bytes. These paths are
  used to perform corrections on the context;
* ``actions``: a list of actions to perform, each action is associated
  with an amount of muduns, and a list of contracts.

The following actions are available:

* The ``unfreeze`` action can be used to unfreeze an account that has
  been frozen because it was not delegated;
* The ``undelegate`` action can be used to remove all the delegations
  of a list of bakers;
* The ``add_allowed_baker`` action can be used to add new allowed bakers;
* The ``remove_allowed_baker`` action can be used to remove allowed
  bakers;
* The ``add_superadmin`` action can be used to add new superadmins;
* The ``remove_superadmin`` action can be used to remove former
  superadmins;
* The ``kyc_accounts`` action can be used to change the KYC of
  accounts when the KYC level is not ``open``;

Inject with ``dune_client``
~~~~~~~~~~~~~~~~~~~~~~~~~~~

To inject the ``Manage_accounts`` operation, the following command
can be used::

  dune-client dune manage accounts with key SUPERADMIN and parameters JSONFILE

  
