Building locally using Docker
=============================

Sometimes, you want to build this local copy of dune-network using a
Docker image to generate Docker binaries. These scripts will help you.

Use `./1_enter_docker.sh` to create a Docker image suitable for
building Dune network. The image is called `dune-network:current`.
The script renames any `_opam` directory to `_opam_old` to avoid
conflicts with the Docker image opam setting.

Use `./2_docker_bash.sh` to create a shell in the Docker image.
Once in the image:

     # you are in /home/$USER
     cd dune-network
     eval `opam env`
     make

Use `./3_leave_docker.sh` to restore the _opam local switch.

