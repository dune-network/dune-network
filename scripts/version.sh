#! /bin/sh

## `ocaml-version` should be in sync with `README.rst` and
## `lib.protocol-compiler/tezos-protocol-compiler.opam`

ocaml_version=4.07.1
opam_version=2

## Please update `.gitlab-ci.yml` accordingly
opam_repository_tag=83d9eb8b05217a340ed2bff64f901537a67385de
full_opam_repository_tag=2881d95ee966e7b3fc8a07cc816b9f525c8272a0
opam_repository_url=https://gitlab.com/dune-network/opam-repository.git
opam_repository=$opam_repository_url\#$opam_repository_tag
