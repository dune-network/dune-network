#!/bin/sh

export COMMAND_NAME

source $SCRIPTS_DIR/default-variables.sh

if [ "$DUNE_ENV" != "" ]; then
    echo Executing $DUNE_ENV
    source $DUNE_ENV || exit 2
elif [ -f $HOME/dune-env.sh ]; then
    echo Executing $HOME/dune-env.sh
    source $HOME/dune-env.sh || exit 2
else
    echo 'You can create $HOME/dune-env.sh to configure your install'
    echo 'or set the DUNE_ENV environment variable'
    echo Default is:
    echo '<<<<<<<<<<<<<<<<<'
    cat $SCRIPTS_DIR/default-variables.sh
    echo '>>>>>>>>>>>>>>>>>'
    exit 2
fi

DUNE_DATADIR=$DUNE_EXECDIR/node
DUNE_LOGDIR=$DUNE_EXECDIR/log
DUNE_RUNDIR=$DUNE_EXECDIR/run
DUNE_CMDDIR=$DUNE_EXECDIR/cmd
DUNE_CLIDIR=$DUNE_EXECDIR/client

if [ -z "$PROCESS_NAME" ]; then
    PROCESS_NAME=$COMMAND_NAME
fi

PIDFILE=$DUNE_RUNDIR/$PROCESS_NAME.pid
LOGFILE=$DUNE_LOGDIR/$PROCESS_NAME.log
INIFILE=$DUNE_DATADIR/version.json


export DUNE_SRCDIR
export DUNE_DATADIR
export DUNE_LOGDIR
export DUNE_RUNDIR
export DUNE_CMDDIR

export SCRIPTS_DIR

export PIDFILE
export LOGFILE
export INIFILE
