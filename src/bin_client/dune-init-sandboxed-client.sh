#! /usr/bin/env bash

dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
tmp_script="$(mktemp -t -p $dir dune-tmp-script.XXXXXXXX)"

cleanup () {
    set +e
    rm -f $tmp_script
}
trap cleanup EXIT INT

sed 's/tezos/dune/g' $dir/tezos-init-sandboxed-client.sh > $tmp_script
chmod +x $tmp_script
export DUNE_GENESIS_PROTOCOL_REVISION=max
$tmp_script $@
