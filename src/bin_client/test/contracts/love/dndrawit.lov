#love

type point = {
  x: nat;
  y: nat;
}

type param_color = {
  r : nat;
  g : nat;
  b : nat;
  a : nat;
}

type pt_color = point * param_color

type color = nat

type image = (nat, color * dun) bigmap

type storage = {
  image: image;
  size: nat;
  ending: timestamp;
}

val%init storage (size : nat) = {
  image = BigMap.empty [:nat] [:color * dun];
  size;
  ending = Current.time () +:! (6 * 31 * 24 * 3600); (* in 6 months *)
}

val%entry main storage d (parameter: pt_color list) =

  if Current.time () > [:timestamp] storage.ending then
    failwith [:string] "The image bid contract is now closed" [:unit];

  let len = List.length [:pt_color] parameter in

  let pixel_amount = match Current.amount () /$+ len with
    | None -> 0dn
    | Some (a, _) -> a in

  let size = storage.size in

  let image = List.fold [:pt_color] [:image]
      (fun ((pt, pcolor) : pt_color) (image : image) ->

         (* Check coordinates withing image *)
         if pt.x >= [:nat] size || pt.y > [:nat] size then
           failwith [:string * nat]
             ("Invalid coordinates, image has square size", size) [:unit];

         let pt = pt.x *+ size ++ pt.y in

         (* Check valid color *)
         if pcolor.r > 255p || pcolor.g > 255p ||
            pcolor.b > 255p || pcolor.a > 255p then
           failwith [:string]
             "Invalid color, must be an RGBA value with each component \
              between 0 and 255" [:unit];

         let old_r, old_g, old_b, min_amount =
           match BigMap.find [:nat] [:color * dun] pt storage.image with
           | None ->
               (255p, 255p, 255p (* white *), 0.01dn)
           | Some (old_color, prev_amount) ->
               (old_color nlsr 16p) nland 255p,
               (old_color nlsr 8p) nland 255p,
               old_color nland 255p,
               prev_amount +$ 0.01dn
         in

         if pixel_amount < [:dun] min_amount then
           failwith [:string * dun]
             ("Whole amount must be greater than this value to \
               update this pixel", (min_amount *$+ len)) [:unit];

         let alpha = pcolor.a in

         let new_r, new_g, new_b =
           (* Perform alpha blending between old color and new color *)
           if alpha = 255p then pcolor.r, pcolor.g, pcolor.b
           else if alpha = 0p then old_r, old_g, old_b
           else
             (* calls to function are inlined for gas *)
             let trans = match Nat.of_int (255p -+ alpha) with
               | None -> 255p
               | Some t -> t in

             let blend (c : nat) (old_c : nat) =
               match (c *+ alpha) /+ 255p with
               | None -> failwith [:unit] () [:nat]
               | Some (c1, _) -> match (old_c *+ trans) /+ 255p with
                 | None -> failwith [:unit] () [:nat]
                 | Some (c2, _) -> c1 ++ c2
             in

             (* red channel alpha blending *)
             let r = blend pcolor.r old_r in

             (* green channel alpha blending *)
             let g = blend pcolor.g old_g in

             (* blue channel alpha blending *)
             let b = blend pcolor.b old_b in

             (r, g, b) in

         (* Encode RGB to 24 bits integer *)
         let color = (new_r nlsl 16p) nlor (new_g nlsl 8p) nlor new_b in
         BigMap.add [:nat] [:color * dun] pt (color, pixel_amount) image

      ) parameter storage.image
  in
  [] [:operation], { storage with image }
