(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Client_context

module Baker : sig
  type args = {
    minimal_fees : string option;
    minimal_nanotez_per_gas_unit : Z.t option;
    minimal_nanotez_per_byte : Z.t option;
    await_endorsements : bool option;
    max_priority : int option;
    context_path : string;
    delegates : Signature.Public_key_hash.t list;
    lazy_baker : int option;
  }

  module Daemons : Dune_daemons.S with type args := args

  val run :
    full ->
    chain:Chain_services.chain ->
    ?protocols:Protocol_hash.t list ->
    ?minimal_fees:string ->
    ?minimal_nanotez_per_gas_unit:Z.t ->
    ?minimal_nanotez_per_byte:Z.t ->
    ?await_endorsements:bool ->
    ?max_priority:int ->
    ?lazy_baker:int ->
    context_path:string ->
    Signature.Public_key_hash.t list ->
    unit tzresult Lwt.t
end

module Endorser : sig
  type args = {delay : int; delegates : Signature.Public_key_hash.t list}

  module Daemons : Dune_daemons.S with type args := args

  val run :
    full ->
    chain:Chain_services.chain ->
    ?protocols:Protocol_hash.t list ->
    delay:int ->
    Signature.Public_key_hash.t list ->
    unit tzresult Lwt.t
end

module Accuser : sig
  type args = {preserved_levels : int}

  module Daemons : Dune_daemons.S with type args := args

  val run :
    full ->
    chain:Chain_services.chain ->
    ?protocols:Protocol_hash.t list ->
    preserved_levels:int ->
    unit tzresult Lwt.t
end
