(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module Baker = struct
  type args = {
    minimal_fees : string option;
    minimal_nanotez_per_gas_unit : Z.t option;
    minimal_nanotez_per_byte : Z.t option;
    await_endorsements : bool option;
    max_priority : int option;
    context_path : string;
    delegates : Signature.Public_key_hash.t list;
    lazy_baker : int option;
  }

  module Daemons = Dune_daemons.Make (struct
    type nonrec args = args

    let name = "baker"

    let cleanup_testchain_nonces = true

    let monitor_all_valid_blocks = false

    let desc_of_args args =
      Dune_node_manager.Baker
        {
          delegates = Signature.Public_key_hash.Set.of_list args.delegates;
          minimal_fees = args.minimal_fees;
          minimal_nanotez_per_gas_unit = args.minimal_nanotez_per_gas_unit;
          minimal_nanotez_per_byte = args.minimal_nanotez_per_byte;
          await_endorsements = args.await_endorsements;
          max_priority = args.max_priority;
        }
  end)

  let run (cctxt : #Client_context.full) ~chain ?protocols ?minimal_fees
      ?minimal_nanotez_per_gas_unit ?minimal_nanotez_per_byte
      ?await_endorsements ?max_priority ?lazy_baker ~context_path delegates =
    Daemons.run
      cctxt
      ~chain
      ?protocols
      {
        minimal_fees;
        minimal_nanotez_per_gas_unit;
        minimal_nanotez_per_byte;
        await_endorsements;
        max_priority;
        context_path;
        delegates;
        lazy_baker;
      }
end

module Endorser = struct
  type args = {delay : int; delegates : Signature.Public_key_hash.t list}

  module Daemons = Dune_daemons.Make (struct
    type nonrec args = args

    let name = "endorser"

    let cleanup_testchain_nonces = false

    let monitor_all_valid_blocks = false

    let desc_of_args args =
      Dune_node_manager.Endorser
        {
          delegates = Signature.Public_key_hash.Set.of_list args.delegates;
          delay = Some args.delay;
        }
  end)

  let run (cctxt : #Client_context.full) ~chain ?protocols ~delay delegates =
    Daemons.run cctxt ~chain ?protocols {delay; delegates}
end

module Accuser = struct
  type args = {preserved_levels : int}

  module Daemons = Dune_daemons.Make (struct
    type nonrec args = args

    let name = "accuser"

    let cleanup_testchain_nonces = true

    let monitor_all_valid_blocks = true

    let desc_of_args args =
      Dune_node_manager.Accuser {preserved_levels = Some args.preserved_levels}
  end)

  let run (cctxt : #Client_context.full) ~chain ?protocols ~preserved_levels =
    Daemons.run cctxt ~chain ?protocols {preserved_levels}
end
