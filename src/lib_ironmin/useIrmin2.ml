(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Path = Irmin.Path.String_list
module Metadata = Irmin.Metadata.None

exception TODO of string

let todo fmt = Fmt.kstrf (fun s -> raise (TODO s)) fmt

module Hash : sig
  include Irmin.Hash.S

  val to_context_hash : t -> Context_hash.t

  val of_context_hash : Context_hash.t -> t
end = struct
  module H = Digestif.Make_BLAKE2B (struct
    let digest_size = 32
  end)

  type t = H.t

  let of_context_hash s = H.of_raw_string (Context_hash.to_string s)

  let to_context_hash h = Context_hash.of_string_exn (H.to_raw_string h)

  let pp ppf t = Context_hash.pp ppf (to_context_hash t)

  let of_string x =
    match Context_hash.of_b58check x with
    | Ok x ->
        Ok (of_context_hash x)
    | Error err ->
        Error
          (`Msg
            (Format.asprintf
               "Failed to read b58check_encoding data: %a"
               Error_monad.pp_print_error
               err))

  let short_hash t = Irmin.Type.(short_hash string (H.to_raw_string t))

  let t : t Irmin.Type.t =
    Irmin.Type.map
      ~cli:(pp, of_string)
      Irmin.Type.(string_of (`Fixed H.digest_size))
      ~short_hash
      H.of_raw_string
      H.to_raw_string

  let hash_size = H.digest_size

  let hash = H.digesti_string
end

module Node = struct
  module M = Irmin.Private.Node.Make (Hash) (Path) (Metadata)

  module V1 = struct
    module Hash = Irmin.Hash.V1 (Hash)

    type kind = [`Node | `Contents of Metadata.t]

    type entry = {kind : kind; name : M.step; node : Hash.t}

    (* Irmin 1.4 uses int64 to store string lengths *)
    let step_t =
      let pre_hash = Irmin.Type.(pre_hash (string_of `Int64)) in
      Irmin.Type.like M.step_t ~pre_hash

    let metadata_t =
      let some = "\255\000\000\000\000\000\000\000" in
      let none = "\000\000\000\000\000\000\000\000" in
      Irmin.Type.(map (string_of (`Fixed 8)))
        (fun s ->
          match s.[0] with
          | '\255' ->
              None
          | '\000' ->
              Some ()
          | _ ->
              assert false)
        (function Some _ -> some | None -> none)

    (* Irmin 1.4 uses int64 to store list lengths *)
    let entry_t : entry Irmin.Type.t =
      let open Irmin.Type in
      record "Tree.entry" (fun kind name node ->
          let kind = match kind with None -> `Node | Some m -> `Contents m in
          {kind; name; node})
      |+ field "kind" metadata_t (function
             | {kind = `Node; _} ->
                 None
             | {kind = `Contents m; _} ->
                 Some m)
      |+ field "name" step_t (fun {name; _} -> name)
      |+ field "node" Hash.t (fun {node; _} -> node)
      |> sealr

    let entries_t : entry list Irmin.Type.t =
      Irmin.Type.(list ~len:`Int64 entry_t)

    let import_entry (s, v) =
      match v with
      | `Node h ->
          {name = s; kind = `Node; node = h}
      | `Contents (h, m) ->
          {name = s; kind = `Contents m; node = h}

    let import t = List.map import_entry (M.list t)

    let pre_hash entries = Irmin.Type.pre_hash entries_t entries
  end

  include M

  let pre_hash_v1 x = V1.pre_hash (V1.import x)

  let t = Irmin.Type.(like t ~pre_hash:pre_hash_v1)
end

module Commit = struct
  module M = Irmin.Private.Commit.Make (Hash)
  module V1 = Irmin.Private.Commit.V1 (M)
  include M

  let pre_hash_v1 t = Irmin.Type.pre_hash V1.t (V1.import t)

  let t = Irmin.Type.like t ~pre_hash:pre_hash_v1
end

module Contents = struct
  type t = string

  let pre_hash_v1 x =
    let ty = Irmin.Type.(pair (string_of `Int64) unit) in
    Irmin.Type.(pre_hash ty) (x, ())

  let t = Irmin.Type.(like ~pre_hash:pre_hash_v1 string)

  let merge = Irmin.Merge.(idempotent (Irmin.Type.option t))
end

module Conf = struct
  let entries = 32

  let stable_hash = 256
end

module OldStore =
  Irmin_pack.Make_ext (Conf) (Irmin.Metadata.None) (Contents)
    (Irmin.Path.String_list)
    (Irmin.Branch.String)
    (Hash)
    (Node)
    (Commit)
module P = OldStore.Private

module Store = struct
  include OldStore

  let repo_close repo = Repo.close repo

  let tree_empty () = Tree.empty

  let tree_hash _repo c =
    match c with
    | `Node _ ->
        Lwt.return (`Node (Tree.hash c))
    | `Contents _ ->
        Lwt.return (`Contents (Tree.hash c, ()))

  let tree_clear ?depth tree = Tree.clear ?depth tree

  let commit_tree commit = Lwt.return (Commit.tree commit)

  let commit_v repo ~info ~parents tree =
    let parents =
      List.map (function `Hash h -> h | _ -> assert false) parents
    in
    Commit.v repo ~info ~parents tree
end

let current_dir = ref "test-db.irmin2"

module Irmin = struct
  module Info = Irmin.Info

  type config = Irmin.config

  let config ?mapsize:_ ?readonly ?index_log_size dir =
    (let maybe_mapfile = Filename.concat dir "mapfile.bin" in
     if Sys.file_exists maybe_mapfile then (
       Printf.eprintf
         "Storage error: file %S exists, showing that you started your node \
          with Ironmin, but you are now running in Irmin2 mode. Use \
          DUNE_CONTEXT_STORAGE=ironmin to start your node in Ironmin mode. If \
          you really want to switch back to Irmin2, use the \
          /storage/context/revert RPC to convert the database back to Irmin2 \
          format.\n\
          %!"
         maybe_mapfile ;
       exit 2 )) ;
    current_dir := dir ;
    Irmin_pack.config ?readonly ?index_log_size dir
end

(* context -> context.parents context.tree *)
let hash ~time ?(message = "") ~parents ~tree =
  let info =
    Irmin.Info.v ~date:(Time.Protocol.to_seconds time) ~author:"Tezos" message
  in
  let parents = List.map (fun c -> Store.Commit.hash c) parents in
  let node = Store.Tree.hash tree in
  let commit = P.Commit.Val.v ~parents ~node ~info in
  let x = P.Commit.Key.hash commit in
  Hash.to_context_hash x

let gc _repo ~genesis:_ ~current:_ = false

let revert _repo = ()

let clear_stats () = ()

let print_stats _repo = ()

let init () = ()

let storage_dir _repo = !current_dir

module MEMCACHE = struct
  type t =
    Store.Repo.t * [`Read | `Write] P.Contents.t * [`Read | `Write] P.Node.t

  let create repo f = P.Repo.batch repo (fun x y _ -> f (repo, x, y))

  let add_string (_, x, _y) string =
    Store.save_contents x string
    >|= fun _ ->
    ignore (Store.Tree.of_contents string) ;
    ()

  let contents_of_hash (repo, _, _) hash = Store.Contents.of_hash repo hash

  let tree_of_hash (repo, _, _) hash = Store.Tree.of_hash repo hash

  let save_tree (repo, x, y) tree =
    (* Save the node in the store ... *)
    Store.save_tree ~clear:true repo x y tree >|= fun _ -> ()

  let add_blob_hash memcache tree key hash =
    contents_of_hash memcache hash
    >>= function
    | None ->
        Lwt.return_none
    | Some v ->
        Store.Tree.add tree key v >>= Lwt.return_some

  let add_node_hash memcache tree key hash =
    tree_of_hash memcache hash
    >>= function
    | None ->
        Lwt.return_none
    | Some t ->
        Store.Tree.add_tree tree key (t :> Store.tree) >>= Lwt.return_some

  let add_dir batch l =
    let rec fold_list sub_tree = function
      | [] ->
          Lwt.return_some sub_tree
      | (step, hash) :: tl -> (
        match hash with
        | `Contents (hash, _) -> (
            add_blob_hash batch sub_tree [step] hash
            >>= function
            | None -> Lwt.return_none | Some sub_tree -> fold_list sub_tree tl
            )
        | `Node hash -> (
            add_node_hash batch sub_tree [step] hash
            >>= function
            | None -> Lwt.return_none | Some sub_tree -> fold_list sub_tree tl
            ) )
    in
    fold_list (Store.tree_empty ()) l
    >>= function
    | None ->
        Lwt.return_none
    | Some tree ->
        save_tree batch tree >|= fun () -> Some tree
end

module I = struct
  let tree_hash _repo = function
    | `Node _ as tree ->
        `Node (Store.Tree.hash tree)
    | `Contents (b, m) ->
        `Contents (Store.Contents.hash b, m)

  let sub_tree tree key = Store.Tree.find_tree tree key

  let tree_list tree = Store.Tree.list tree []

  let tree_content tree = Store.Tree.find tree []
end

let fold_tree_path ?(progress = fun _ -> ()) repo tree f =
  (* Noting the visited hashes *)
  let visited_hash = Hashtbl.create 1000 in
  let visited h = Hashtbl.mem visited_hash h in
  let set_visit h = Hashtbl.add visited_hash h () in
  let cpt = ref 0 in
  let rec fold_tree_path tree =
    I.tree_list tree
    >>= fun keys ->
    let keys = List.sort (fun (a, _) (b, _) -> String.compare a b) keys in
    Lwt_list.map_s
      (fun (name, kind) ->
        I.sub_tree tree [name]
        >>= function
        | None ->
            assert false
        | Some sub_tree ->
            let hash = I.tree_hash repo sub_tree in
            ( if visited hash then Lwt.return_unit
            else (
              progress !cpt ;
              incr cpt ;
              set_visit hash ;
              (* There cannot be a cycle *)
              match kind with
              | `Node ->
                  fold_tree_path sub_tree
              | `Contents -> (
                  I.tree_content sub_tree
                  >>= function
                  | None -> assert false | Some data -> f (`Data data) ) ) )
            >|= fun () -> (name, hash))
      keys
    >>= fun sub_keys -> f (`Node sub_keys)
  in
  fold_tree_path tree

(* unshalow possible 1-st level objects from previous partial
   checkouts ; might be better to pass directly the list of shallow
   objects. *)
let unshallow repo tree =
  Store.Tree.list tree []
  >>= fun childs ->
  P.Repo.batch repo (fun x y _ ->
      Lwt_list.iter_s
        (fun (s, k) ->
          match k with
          | `Contents ->
              Lwt.return ()
          | `Node ->
              Store.Tree.get_tree tree [s]
              >>= fun tree ->
              Store.save_tree ~clear:true repo x y tree >|= fun _ -> ())
        childs)

let raw_commit info (repo, parents, tree) =
  let parents = List.map Store.Commit.hash parents in
  let parents = List.map (fun h -> `Hash h) parents in
  unshallow repo tree
  >>= fun () ->
  Store.commit_v repo ~info ~parents tree
  >|= fun h -> Store.Tree.clear tree ; h

let context_parents _repo commit =
  let parents = Store.Commit.parents commit in
  let parents = List.map Hash.to_context_hash parents in
  Lwt.return (List.sort Context_hash.compare parents)

let set_context _batch ~info ~parents (repo, tree) bh =
  let parents = List.map (fun h -> `Hash h) parents in
  Store.commit_v repo ~info ~parents tree
  >>= fun c ->
  let ctxt_h = Store.Commit.hash c in
  if
    Context_hash.equal
      bh.Block_header.shell.context
      (Hash.to_context_hash ctxt_h)
  then Lwt.return_some bh
  else Lwt.return_none

let compute_context_hash repo tree ~info ~parents_hashes data_hash =
  let data_tree = Store.Tree.shallow repo data_hash in
  Store.Tree.add_tree tree ["data"] data_tree
  >>= fun node ->
  let node = Store.Tree.hash node in
  let commit = P.Commit.Val.v ~parents:parents_hashes ~node ~info in
  let computed_context_hash =
    Hash.to_context_hash (P.Commit.Key.hash commit)
  in
  let mock_parents = [Store.of_private_commit repo commit] in
  let commit_parents = List.map (fun c -> `Hash c) parents_hashes in
  Lwt.return
    ( computed_context_hash,
      mock_parents,
      Store.Tree.shallow repo data_hash,
      commit_parents )
