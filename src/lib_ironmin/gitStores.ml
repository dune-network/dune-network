(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

module Ironmin = Ocplib_ironmin.Ironmin

let debug_iron = Ironmin.if_var_exists "IRONMIN_DEBUG"

let debug_api = Ironmin.if_var_exists "IRONMIN_DEBUG_API"

let record_size = Ironmin.if_var_exists "IRONMIN_RECORD_SIZE"

let reads_also = Ironmin.if_var_exists "IRONMIN_READS_ALSO"

let max_tree_diff = Ironmin.int_var_or "IRONMIN_MAX_TREE_DIFF" 150_000

let no_progress = Ironmin.if_var_exists "IRONMIN_NO_PROGRESS"

let max_steps = Ironmin.int_var_or "IRONMIN_MAX_STEPS" max_int

let gc_every = Ironmin.int_var_or "IRONMIN_GC_CYCLES" max_int

let cycle_length = Ironmin.int_var_or "IRONMIN_CYCLE_LENGTH" 4096

let print_operations = Ironmin.if_var_exists "IRONMIN_PRINT"

let max_recorded_string_length = 10

(* This modules combines two Store, calling both of them at every
   call, and checks if they return the same results. Only useful for
   two compatible Stores, such as Irmin and Ironmin with Irmin hash
   compatibility.
*)

module Combine (A : Irmin_sig.S) (B : Irmin_sig.S) : Irmin_sig.S = struct
  let init () = A.init () ; B.init ()

  module Hash = struct
    type t = A.Hash.t * B.Hash.t

    let of_context_hash h = (A.Hash.of_context_hash h, B.Hash.of_context_hash h)

    let to_context_hash h = A.Hash.to_context_hash (fst h)
  end

  module Irmin = struct
    type config = A.Irmin.config * B.Irmin.config

    module Info = struct
      type t = A.Irmin.Info.t * B.Irmin.Info.t

      let v ~date ~author message =
        let a = A.Irmin.Info.v ~date ~author message in
        let b = B.Irmin.Info.v ~date ~author message in
        (a, b)

      let message (t1, t2) =
        let m1 = A.Irmin.Info.message t1 in
        let m2 = B.Irmin.Info.message t2 in
        assert (m1 = m2) ;
        m1

      let author (t1, t2) =
        let m1 = A.Irmin.Info.author t1 in
        let m2 = B.Irmin.Info.author t2 in
        assert (m1 = m2) ;
        m1

      let date (t1, t2) =
        let m1 = A.Irmin.Info.date t1 in
        let m2 = B.Irmin.Info.date t2 in
        assert (m1 = m2) ;
        m1
    end

    let config ?mapsize ?readonly ?index_log_size root =
      let a_config =
        A.Irmin.config ?mapsize ?readonly ?index_log_size (root ^ "-A")
      in
      let b_config =
        B.Irmin.config ?mapsize ?readonly ?index_log_size (root ^ "-B")
      in
      (a_config, b_config)
  end

  module Store = struct
    type hash = Hash.t

    type tree = A.Store.tree * B.Store.tree

    module Tree = struct
      let mem (a_tree, b_tree) path =
        A.Store.Tree.mem a_tree path
        >>= function
        | a_res -> (
            B.Store.Tree.mem b_tree path
            >>= function
            | b_res ->
                assert (a_res = b_res) ;
                Lwt.return a_res )

      let mem_tree (a_tree, b_tree) path =
        A.Store.Tree.mem_tree a_tree path
        >>= function
        | a_res -> (
            B.Store.Tree.mem_tree b_tree path
            >>= function
            | b_res ->
                assert (a_res = b_res) ;
                Lwt.return a_res )

      let find (a_tree, b_tree) path =
        A.Store.Tree.find a_tree path
        >>= function
        | a_res -> (
            B.Store.Tree.find b_tree path
            >>= function
            | b_res ->
                ( match (a_res, b_res) with
                | (None, None) ->
                    ()
                | (None, Some _) ->
                    assert false
                | (Some _, None) ->
                    assert false
                | (Some a_res, Some b_res) ->
                    if a_res <> b_res then (
                      Printf.eprintf "a_res: %S\n%!" a_res ;
                      Printf.eprintf "b_res: %S\n%!" b_res ;
                      assert false ) ) ;
                Lwt.return a_res )

      let remove (a_tree, b_tree) path =
        A.Store.Tree.remove a_tree path
        >>= function
        | a_res -> (
            B.Store.Tree.remove b_tree path
            >>= function b_res -> Lwt.return (a_res, b_res) )

      let add (a_tree, b_tree) path ?metadata value =
        assert (metadata = None) ;
        A.Store.Tree.add a_tree path value
        >>= function
        | a_res -> (
            B.Store.Tree.add b_tree path value
            >>= function b_res -> Lwt.return (a_res, b_res) )

      let add_tree (a_tree, b_tree) path (a_value, b_value) =
        A.Store.Tree.add_tree a_tree path a_value
        >>= function
        | a_res -> (
            B.Store.Tree.add_tree b_tree path b_value
            >>= function b_res -> Lwt.return (a_res, b_res) )

      let find_tree (a_tree, b_tree) path =
        A.Store.Tree.find_tree a_tree path
        >>= function
        | a_res -> (
            B.Store.Tree.find_tree b_tree path
            >>= function
            | b_res ->
                let res =
                  match (a_res, b_res) with
                  | (None, None) ->
                      None
                  | (Some a_res, Some b_res) ->
                      Some (a_res, b_res)
                  | (None, Some _) ->
                      assert false
                  | (Some _, None) ->
                      assert false
                in
                Lwt.return res )

      let list (a_tree, b_tree) path =
        A.Store.Tree.list a_tree path
        >>= function
        | a_list -> (
            B.Store.Tree.list b_tree path
            >>= function
            | b_list ->
                assert (a_list = b_list) ;
                Lwt.return a_list )
    end

    module Repo = struct
      type t = A.Store.Repo.t * B.Store.Repo.t

      let v (a_config, b_config) =
        A.Store.Repo.v a_config
        >>= function
        | a_t -> (
            B.Store.Repo.v b_config >>= function b_t -> Lwt.return (a_t, b_t) )
    end

    module Commit = struct
      type t = A.Store.Commit.t * B.Store.Commit.t

      let of_hash (a_repo, b_repo) (a_hash, b_hash) =
        A.Store.Commit.of_hash a_repo a_hash
        >>= function
        | a_res -> (
            B.Store.Commit.of_hash b_repo b_hash
            >>= function
            | b_res ->
                let res =
                  match (a_res, b_res) with
                  | (None, None) ->
                      None
                  | (Some a_t, Some b_t) ->
                      Some (a_t, b_t)
                  | (None, Some _) ->
                      assert false
                  | (Some _, None) ->
                      assert false
                in
                Lwt.return res )

      let info (a_commit, b_commit) =
        let a_info = A.Store.Commit.info a_commit in
        let b_info = B.Store.Commit.info b_commit in
        (a_info, b_info)

      let hash (a_t, b_t) =
        let a_res = A.Store.Commit.hash a_t in
        let b_res = B.Store.Commit.hash b_t in
        assert (A.Hash.to_context_hash a_res = B.Hash.to_context_hash b_res) ;
        (a_res, b_res)
    end

    module Branch = struct
      let set (a_repo, _) branch_name (a_commit, _) =
        A.Store.Branch.set a_repo branch_name a_commit

      let remove (a_repo, _) branch_name =
        A.Store.Branch.remove a_repo branch_name

      let master = A.Store.Branch.master
    end

    let repo_close (a_repo, b_repo) =
      A.Store.repo_close a_repo >>= fun () -> B.Store.repo_close b_repo

    let tree_empty () = (A.Store.tree_empty (), B.Store.tree_empty ())

    let tree_hash (a_repo, b_repo) (a_tree, b_tree) =
      A.Store.tree_hash a_repo a_tree
      >>= fun a_h ->
      B.Store.tree_hash b_repo b_tree
      >>= fun b_h ->
      match (a_h, b_h) with
      | (`Node a_n, `Node b_n) ->
          assert (A.Hash.to_context_hash a_n = B.Hash.to_context_hash b_n) ;
          Lwt.return (`Node (a_n, b_n))
      | (`Contents (a_n, _), `Contents (b_n, _)) ->
          assert (A.Hash.to_context_hash a_n = B.Hash.to_context_hash b_n) ;
          Lwt.return (`Contents ((a_n, b_n), ()))
      | _ ->
          assert false

    let tree_clear ?depth (a_tree, b_tree) =
      A.Store.tree_clear ?depth a_tree ;
      B.Store.tree_clear ?depth b_tree ;
      ()

    let commit_tree (a_commit, b_commit) =
      A.Store.commit_tree a_commit
      >>= function
      | a_tree -> (
          B.Store.commit_tree b_commit
          >>= function b_tree -> Lwt.return (a_tree, b_tree) )

    let commit_v (a_repo, b_repo) ~info:(a_info, b_info) ~parents
        (a_tree, b_tree) =
      let parents =
        List.map
          (function
            | `Commit (a_c, b_c) ->
                (`Commit a_c, `Commit b_c)
            | `Hash (a_c, b_c) ->
                (`Hash a_c, `Hash b_c)
            | _ ->
                assert false)
          parents
      in
      let (a_parents, b_parents) = List.split parents in
      A.Store.commit_v a_repo ~info:a_info ~parents:a_parents a_tree
      >>= function
      | a_res -> (
          B.Store.commit_v b_repo ~info:b_info ~parents:b_parents b_tree
          >>= function b_res -> Lwt.return (a_res, b_res) )
  end

  let hash ~time ?message ~parents ~tree =
    let (a_tree, b_tree) = tree in
    let (a_parents, b_parents) = List.split parents in
    let a = A.hash ~time ?message ~parents:a_parents ~tree:a_tree in
    let b = B.hash ~time ?message ~parents:b_parents ~tree:b_tree in
    assert (a = b) ;
    a

  let storage_dir (a_repo, _) = A.storage_dir a_repo

  let gc (a_repo, b_repo) ~genesis:(a_genesis, b_genesis)
      ~current:(a_current, b_current) =
    let a = A.gc a_repo ~genesis:a_genesis ~current:a_current in
    let b = B.gc b_repo ~genesis:b_genesis ~current:b_current in
    a || b

  let revert (a_repo, b_repo) = A.revert a_repo ; B.revert b_repo ; ()

  let clear_stats () = A.clear_stats () ; B.clear_stats () ; ()

  let print_stats (a_repo, b_repo) =
    A.print_stats a_repo ; B.print_stats b_repo ; ()

  module MEMCACHE = struct
    type t = A.MEMCACHE.t * B.MEMCACHE.t

    let create (a_repo, b_repo) f =
      A.MEMCACHE.create a_repo (fun a_batch ->
          B.MEMCACHE.create b_repo (fun b_batch -> f (a_batch, b_batch)))

    let add_string (a_batch, b_batch) string =
      A.MEMCACHE.add_string a_batch string
      >>= fun () -> B.MEMCACHE.add_string b_batch string

    let add_dir (a_batch, b_batch) l =
      let l =
        List.map
          (function
            | (s, `Contents ((a_c, b_c), _)) ->
                ((s, `Contents (a_c, ())), (s, `Contents (b_c, ())))
            | (s, `Node (a_h, b_h)) ->
                ((s, `Node a_h), (s, `Node b_h)))
          l
      in
      let (a_l, b_l) = List.split l in
      A.MEMCACHE.add_dir a_batch a_l
      >>= fun a_res ->
      B.MEMCACHE.add_dir b_batch b_l
      >|= fun b_res ->
      match (a_res, b_res) with
      | (None, None) ->
          None
      | (Some a, Some b) ->
          Some (a, b)
      | _ ->
          assert false
  end

  let fold_tree_path ?progress (a_repo, b_repo) (a_tree, b_tree) f =
    let a_l = ref [] in
    let b_l = ref [] in
    let a_f x =
      a_l := x :: !a_l ;
      Lwt.return ()
    in
    let b_f x =
      b_l := x :: !b_l ;
      Lwt.return ()
    in
    A.fold_tree_path ?progress a_repo a_tree a_f
    >>= fun () ->
    B.fold_tree_path ?progress b_repo b_tree b_f
    >>= fun () ->
    assert (List.length !a_l = List.length !b_l) ;
    let l =
      List.map2
        (fun a_x b_x ->
          match (a_x, b_x) with
          | (`Data a_d, `Data b_d) ->
              assert (a_d = b_d) ;
              `Data a_d
          | (`Node a_l, `Node b_l) ->
              assert (List.length a_l = List.length b_l) ;
              let l =
                List.map2
                  (fun (a_s, a_x) (b_s, b_x) ->
                    assert (a_s = b_s) ;
                    ( a_s,
                      match (a_x, b_x) with
                      | (`Node a_h, `Node b_h) ->
                          assert (
                            Context_hash.equal
                              (A.Hash.to_context_hash a_h)
                              (B.Hash.to_context_hash b_h) ) ;
                          `Node (a_h, b_h)
                      | (`Contents (a_h, ()), `Contents (b_h, ())) ->
                          assert (
                            Context_hash.equal
                              (A.Hash.to_context_hash a_h)
                              (B.Hash.to_context_hash b_h) ) ;
                          `Contents ((a_h, b_h), ())
                      | _ ->
                          assert false ))
                  a_l
                  b_l
              in
              `Node l
          | _ ->
              assert false)
        !a_l
        !b_l
    in
    Lwt_list.iter_s f l

  let raw_commit (a_info, b_info) ((a_repo, b_repo), parents, tree) =
    let (a_parents, b_parents) = List.split parents in
    let (a_tree, b_tree) = tree in
    A.raw_commit a_info (a_repo, a_parents, a_tree)
    >>= fun a_c ->
    B.raw_commit b_info (b_repo, b_parents, b_tree)
    >>= fun b_c -> Lwt.return (a_c, b_c)

  let context_parents (a_ctxt, b_ctxt) (a_commit, b_commit) =
    A.context_parents a_ctxt a_commit
    >>= fun a_chl ->
    B.context_parents b_ctxt b_commit
    >>= fun b_chl ->
    assert (List.length a_chl = List.length b_chl) ;
    assert (List.for_all2 Context_hash.equal a_chl b_chl) ;
    Lwt.return a_chl

  let set_context (a_batch, b_batch) ~info:(a_info, b_info) ~parents
      ((a_repo, b_repo), (a_tree, b_tree)) bh =
    let (a_parents, b_parents) = List.split parents in
    A.set_context a_batch ~info:a_info ~parents:a_parents (a_repo, a_tree) bh
    >>= fun a_bh ->
    B.set_context b_batch ~info:b_info ~parents:b_parents (b_repo, b_tree) bh
    >>= fun b_bh ->
    let bh =
      match (a_bh, b_bh) with
      | (None, None) ->
          None
      | (Some a_bh, Some b_bh) ->
          assert (Block_header.equal a_bh b_bh) ;
          Some a_bh
      | _ ->
          assert false
    in
    Lwt.return bh

  let compute_context_hash (a_repo, b_repo) (a_tree, b_tree)
      ~info:(a_info, b_info) ~parents_hashes (a_data_hash, b_data_hash) =
    let (a_parents_hashes, b_parents_hashes) = List.split parents_hashes in
    A.compute_context_hash
      a_repo
      a_tree
      ~info:a_info
      ~parents_hashes:a_parents_hashes
      a_data_hash
    >>= fun ( a_computed_context_hash,
              a_mock_parents,
              a_data_tree,
              a_commit_parents ) ->
    B.compute_context_hash
      b_repo
      b_tree
      ~info:b_info
      ~parents_hashes:b_parents_hashes
      b_data_hash
    >>= fun ( b_computed_context_hash,
              b_mock_parents,
              b_data_tree,
              b_commit_parents ) ->
    assert (Context_hash.equal a_computed_context_hash b_computed_context_hash) ;
    let mock_parents = List.combine a_mock_parents b_mock_parents in
    let commit_parents =
      List.map2
        (fun a_c b_c ->
          match (a_c, b_c) with
          | (`Commit a_c, `Commit b_c) ->
              `Commit (a_c, b_c)
          | (`Hash a_h, `Hash b_h) ->
              assert (
                Context_hash.equal
                  (A.Hash.to_context_hash a_h)
                  (B.Hash.to_context_hash b_h) ) ;
              `Hash (a_h, b_h)
          | (`Commit _a_c, `Hash _b_h) ->
              assert false
          | (`Hash _a_h, `Commit _b_c) ->
              assert false)
        a_commit_parents
        b_commit_parents
    in
    Lwt.return
      ( a_computed_context_hash,
        mock_parents,
        (a_data_tree, b_data_tree),
        commit_parents )
end

(* This module will record all calls to Store into a file, so that
   we can later replay only these calls, using the
   IRONMIN_TEST environment variable.
   . *)

type op =
  | Info_v of int64 * string * string
  | Tree_empty
  | Tree_mem of int * string list * bool
  | Tree_mem_tree of int * string list * bool
  | Tree_remove of int * string list
  | Tree_find of int * string list * (int * string) option
  | Tree_add of int * string list * string
  | Tree_add_tree of int * string list * int
  | Tree_find_tree of int * string list * bool
  | Tree_list of int * string list * (string * [`Contents | `Node]) list
  | Config of int64 option * bool option * int option * string
  | Commit_tree of int
  | Commit_of_hash of Context_hash.t
  | Commit_v of int * int list * int * Context_hash.t
  | Commit_hash of int * Context_hash.t
  | Hash of int64 * string * int list * int * Context_hash.t
  | Commit_info of int
  | Tree_hash of int * Context_hash.t Irmin_sig.tree_hash

type player =
  ?record_size:bool ->
  ?reads_also:bool ->
  ?max_tree_diff:int ->
  ?no_progress:bool ->
  ?max_steps:int ->
  ?switch:int * (unit -> unit) ->
  trace:string ->
  db:string ->
  unit ->
  unit Lwt.t

module Record (B : Irmin_sig.S) : sig
  include Irmin_sig.S

  val play : player

  val print : trace:string -> int

  val copy :
    ?reads_also:bool -> ?max_steps:int -> src:string -> dst:string -> unit
end = struct
  let init () = B.init ()

  module Hash = B.Hash

  let hex_of_string hash =
    let (`Hex s) = Hex.of_string hash in
    s

  let print_op step op =
    match op with
    | Config (_mapsize, _readonly, _index_log_size, _root) ->
        Printf.printf "%d config\n" step
    | Info_v (_date, _author, _message) ->
        Printf.printf "%d info\n" step
    | Tree_empty ->
        Printf.printf "%d tree_empty\n" step
    | Tree_mem (_num, path, result) ->
        Printf.printf
          "%d tree_mem %s = %b\n"
          step
          (String.concat "." path)
          result
    | Tree_mem_tree (_num, path, result) ->
        Printf.printf
          "%d tree_mem_tree %s = %b\n"
          step
          (String.concat "." path)
          result
    | Tree_remove (_num, path) ->
        Printf.printf "%d tree_remove %s\n" step (String.concat "." path)
    | Tree_find (_num, path, _result) ->
        Printf.printf "%d tree_find %s\n" step (String.concat "." path)
    | Tree_find_tree (_num, path, result) ->
        Printf.printf
          "%d tree_find_tree %s = %b\n"
          step
          (String.concat "." path)
          result
    | Tree_add (_num, path, value) ->
        Printf.printf "%d tree_add %s %S\n" step (String.concat "." path) value
    | Tree_add_tree (_num1, path, _num2) ->
        Printf.printf "%d tree_add_tree %s\n" step (String.concat "." path)
    | Tree_list (_num, path, _list) ->
        Printf.printf "%d tree_list %s\n" step (String.concat "." path)
    | Commit_tree _num ->
        Printf.printf "%d commit_tree\n" step
    | Commit_of_hash hash ->
        Printf.printf
          "%d commit_of_hash %s\n"
          step
          (hex_of_string (Context_hash.to_string hash))
    | Commit_v (_num_info, _parents, _num_tree, _hash) ->
        Printf.printf "%d commit_v\n" step
    | Commit_hash (_num_commit, hash) ->
        let (`Hex s) = Hex.of_string (Context_hash.to_string hash) in
        Printf.printf "%d commit_hash %s\n" step s
    | Hash (_date, _message, _num_parents, _num_tree, _hash) ->
        Printf.printf "%d hash\n" step
    | Commit_info _num ->
        Printf.printf "%d commit_info\n" step
    | Tree_hash (_num, _hash) ->
        Printf.printf "%d tree_hash\n" step

  let play ?(record_size = record_size) ?(reads_also = reads_also)
      ?(max_tree_diff = max_tree_diff) ?(no_progress = no_progress)
      ?(max_steps = max_steps) ?switch ~trace:filename ~db:db_name () =
    let writes_only = not reads_also in
    let progress = not no_progress in
    let ic = open_in_bin filename in
    let on_exit = ref [] in
    let step = ref 0 in
    let cycle = ref 1 in
    let cycles = Queue.create () in
    B.clear_stats () ;
    let record_size =
      if record_size then (
        let oc = open_out (db_name ^ ".sizes") in
        let t0 = Unix.gettimeofday () in
        let record_size repo commit info =
          let db_name = B.storage_dir repo in
          let db = Filename.concat db_name "data.mdb" in
          let db2 = Filename.concat db_name "mapfile.bin" in
          let h =
            Context_hash.to_string
              (B.Hash.to_context_hash (B.Store.Commit.hash commit))
          in
          let st = (Unix.stat db).Unix.st_size in
          let st2 = try (Unix.stat db2).Unix.st_size with _ -> 0 in
          let gc_stats = Gc.quick_stat () in
          let gc_ctrl = Gc.get () in
          let t1 = Unix.gettimeofday () in
          Printf.fprintf
            oc
            "%s %d %d %d %d %s\n"
            (hex_of_string h)
            (st + st2)
            !step
            (int_of_float (t1 -. t0))
            (gc_stats.Gc.heap_words + gc_ctrl.Gc.minor_heap_size)
            (B.Irmin.Info.message info)
        in
        on_exit := (fun () -> close_out oc) :: !on_exit ;
        record_size )
      else fun _repo _commit _info -> ()
    in
    let new_table name max_diff =
      let table = Hashtbl.create 1111 in
      let counter = ref 0 in
      let record x =
        let num = !counter in
        incr counter ;
        if print_operations then Printf.printf "record %d %s\n%!" num name ;
        if num >= max_diff then Hashtbl.remove table (num - max_diff) ;
        Hashtbl.add table num x
      in
      let get num =
        if print_operations then Printf.printf "get %d %s\n%!" num name ;
        if num < !counter - max_diff then (
          Printf.eprintf "get %d %s at %d\n%!" num name !counter ;
          Printf.eprintf
            "Error: access too far, table max_diff should be at least %d\n%!"
            (!counter - num + 1) ;
          exit 2 ) ;
        match Hashtbl.find table num with
        | exception Not_found ->
            assert false
        | None ->
            assert false
        | Some v ->
            v
      in
      (record, get)
    in
    let (record_info, get_info) = new_table "info" 2 in
    let (record_tree, get_tree) = new_table "tree" max_tree_diff in
    let (record_commit, get_commit) = new_table "commit" 2 in
    let repo = ref None in
    let genesis = ref None in
    let get_repo () =
      match !repo with None -> assert false | Some repo -> repo
    in
    let finish () =
      Printf.eprintf "Finished after %d steps\n%!" !step ;
      close_in ic ;
      B.print_stats (get_repo ()) ;
      List.iter (fun f -> f ()) !on_exit
    in
    let rec iter () =
      if !step = max_steps then (finish () ; Lwt.return_unit)
      else
        match (input_value ic : op) with
        | exception End_of_file ->
            finish () ; Lwt.return_unit
        | exception exn ->
            finish () ; raise exn
        | op -> (
            if print_operations then print_op !step op ;
            ( match switch with
            | Some (nops, f) when nops = !step ->
                f ()
            | _ ->
                () ) ;
            if progress && !step mod 10_000 = 0 then
              Printf.eprintf "executed %d operations\n%!" !step ;
            incr step ;
            match op with
            | Config (mapsize, readonly, index_log_size, _root) ->
                let config =
                  B.Irmin.config ?mapsize ?readonly ?index_log_size db_name
                in
                B.Store.Repo.v config
                >>= fun r ->
                repo := Some r ;
                iter ()
            | Info_v (date, author, message) ->
                let info = B.Irmin.Info.v ~date ~author message in
                record_info (Some info) ; iter ()
            | Tree_mem (num, path, result) ->
                if writes_only then iter ()
                else
                  B.Store.Tree.mem (get_tree num) path
                  >>= fun bool ->
                  assert (result = bool) ;
                  iter ()
            | Tree_mem_tree (num, path, result) ->
                if writes_only then iter ()
                else
                  B.Store.Tree.mem_tree (get_tree num) path
                  >>= fun bool ->
                  assert (result = bool) ;
                  iter ()
            | Tree_find (num, path, result) ->
                if writes_only then iter ()
                else
                  B.Store.Tree.find (get_tree num) path
                  >>= fun value ->
                  ( match (value, result) with
                  | (None, None) ->
                      ()
                  | (Some _, None) ->
                      assert false
                  | (None, Some _) ->
                      assert false
                  | (Some v1, Some (len2, s2)) ->
                      let len1 = String.length v1 in
                      assert (len1 = len2) ;
                      let s1 =
                        if len1 > max_recorded_string_length then
                          String.sub v1 0 (1 + max_recorded_string_length)
                        else v1
                      in
                      if s1 <> s2 then (
                        print_op !step op ;
                        Printf.eprintf "s1 = %S\n%!" s1 ;
                        Printf.eprintf "expected = %S\n%!" s2 ;
                        exit 2 ) ) ;
                  iter ()
            | Tree_list (num, path, result) ->
                if writes_only then iter ()
                else
                  B.Store.Tree.list (get_tree num) path
                  >>= fun value ->
                  if value <> result then (
                    Printf.eprintf
                      "result: %d %s\n"
                      (List.length value)
                      (String.concat
                         ","
                         (List.map
                            (function
                              | (s, `Contents) ->
                                  s ^ "_C"
                              | (s, `Node) ->
                                  s ^ "_N")
                            value)) ;
                    Printf.eprintf
                      "expected: %d %s\n"
                      (List.length result)
                      (String.concat
                         ","
                         (List.map
                            (function
                              | (s, `Contents) ->
                                  s ^ "_C"
                              | (s, `Node) ->
                                  s ^ "_N")
                            result)) ;
                    exit 2 ) ;
                  iter ()
            | Tree_hash (num, `Node hash) -> (
                let tree = get_tree num in
                B.Store.tree_hash (get_repo ()) tree
                >>= function
                | `Node hash2 ->
                    assert (hash = Hash.to_context_hash hash2) ;
                    iter ()
                | `Contents _ ->
                    assert false )
            | Tree_hash (_num, `Contents _) ->
                assert false
            | Tree_empty ->
                let tree = B.Store.tree_empty () in
                record_tree (Some tree) ; iter ()
            | Tree_remove (num, path) ->
                B.Store.Tree.remove (get_tree num) path
                >>= fun tree -> record_tree (Some tree) ; iter ()
            | Tree_find_tree (num, path, bool) ->
                B.Store.Tree.find_tree (get_tree num) path
                >>= fun tree ->
                ( match tree with
                | None ->
                    assert (bool = false)
                | Some _ ->
                    assert bool ) ;
                record_tree tree ; iter ()
            | Tree_add (num, path, value) ->
                B.Store.Tree.add (get_tree num) path value
                >>= fun tree -> record_tree (Some tree) ; iter ()
            | Tree_add_tree (num1, path, num2) ->
                B.Store.Tree.add_tree (get_tree num1) path (get_tree num2)
                >>= fun tree -> record_tree (Some tree) ; iter ()
            | Commit_tree num ->
                let c = get_commit num in
                B.Store.commit_tree c
                >>= fun tree -> record_tree (Some tree) ; iter ()
            | Commit_info num ->
                let c = get_commit num in
                let info = B.Store.Commit.info c in
                record_info (Some info) ; iter ()
            | Commit_of_hash hash ->
                B.Store.Commit.of_hash
                  (get_repo ())
                  (Hash.of_context_hash hash)
                >>= fun commit -> record_commit commit ; iter ()
            | Commit_v (num_info, parents, num_tree, result) -> (
                let info = get_info num_info in
                let parents = List.map get_commit parents in
                let parents = List.map (fun c -> `Commit c) parents in
                (* TODO*)
                let tree = get_tree num_tree in
                let repo = get_repo () in
                B.Store.commit_v repo ~info ~parents tree
                >>= fun commit ->
                assert (
                  B.Store.Commit.hash commit |> Hash.to_context_hash = result
                ) ;
                ( match !genesis with
                | None ->
                    genesis := Some result
                | Some _ ->
                    () ) ;
                let gc_happened =
                  if gc_every < 1000 then
                    let message = B.Irmin.Info.message info in
                    match String.split_on_char ' ' message with
                    | [] ->
                        false
                    | [_] ->
                        false (* "Genesis" *)
                    | "lvl" :: num_comma :: _ ->
                        if
                          num_comma
                          = string_of_int (!cycle * cycle_length) ^ ","
                        then (
                          let hash = B.Store.Commit.hash commit in
                          Printf.eprintf
                            "end_of_cycle: %d %s\n%!"
                            !cycle
                            (hex_of_string
                               (Context_hash.to_string
                                  (Hash.to_context_hash hash))) ;
                          Queue.add (!cycle, hash) cycles ;
                          let gc_happened =
                            if Queue.length cycles > gc_every then (
                              let (cycle, current) = Queue.take cycles in
                              for _i = 2 to gc_every do
                                ignore (Queue.take cycles)
                              done ;
                              Printf.eprintf
                                "Garbage collecting cycle %d\n%!"
                                cycle ;
                              let genesis =
                                match !genesis with
                                | None ->
                                    assert false
                                | Some genesis ->
                                    Hash.of_context_hash genesis
                              in
                              ignore (B.gc repo ~genesis ~current) ;
                              true )
                            else false
                          in
                          cycle := !cycle + 1 ;
                          gc_happened )
                        else false
                    | _ ->
                        false
                  else false
                in
                ( if gc_happened then
                  B.Store.Commit.of_hash repo (B.Store.Commit.hash commit)
                else Lwt.return (Some commit) )
                >>= function
                | None ->
                    assert false
                | Some commit ->
                    record_size repo commit info ;
                    record_commit (Some commit) ;
                    iter () )
            | Commit_hash (num_commit, hash) ->
                if writes_only then iter ()
                else
                  let commit = get_commit num_commit in
                  let h = B.Store.Commit.hash commit in
                  assert (B.Hash.to_context_hash h = hash) ;
                  iter ()
            | Hash (date, message, num_parents, num_tree, hash) ->
                if writes_only then iter ()
                else
                  let parents = List.map get_commit num_parents in
                  let tree = get_tree num_tree in
                  let time = Time.Protocol.of_seconds date in
                  let h = B.hash ~time ~message ~parents ~tree in
                  assert (h = hash) ;
                  iter () )
    in
    Lwt.catch iter (fun exn -> finish () ; raise exn)

  let copy ?(reads_also = reads_also) ?(max_steps = max_steps) ~src ~dst =
    let ic = open_in_bin src in
    let oc = open_out_bin dst in
    let step = ref 0 in
    let copied = ref 0 in
    let finish () =
      Printf.eprintf "Finished after %d steps\n%!" !step ;
      Printf.eprintf "Copied %d operations\n%!" !copied ;
      close_in ic ;
      close_out oc
    in
    let rec iter () =
      if !step = max_steps then finish ()
      else
        match (input_value ic : op) with
        | exception End_of_file ->
            finish ()
        | exception exn ->
            finish () ; raise exn
        | op ->
            incr step ;
            let copy =
              match op with
              | Config _ | Info_v _ ->
                  true
              | Tree_hash _
              | Tree_mem _
              | Tree_mem_tree _
              | Tree_find _
              | Tree_list _ ->
                  reads_also
              | Tree_empty
              | Tree_remove _
              | Tree_find_tree _
              | Tree_add _
              | Tree_add_tree _
              | Commit_tree _
              | Commit_info _
              | Commit_of_hash _
              | Commit_v _ ->
                  true
              | Commit_hash _ | Hash _ ->
                  reads_also
            in
            if copy then (incr copied ; output_value oc op) ;
            iter ()
    in
    iter ()

  let print ~trace:filename =
    let ic = open_in_bin filename in
    let diffs = ref [] in
    let new_table s =
      let counter = ref 0 in
      let max_diff = ref 0 in
      diffs := (s, max_diff) :: !diffs ;
      let record () = incr counter in
      let get num =
        let n = !counter - num in
        if n > !max_diff then max_diff := n
      in
      (record, get)
    in
    let finish steps =
      Printf.eprintf "Done with %d steps\n" steps ;
      List.iter
        (fun (s, max_diff) -> Printf.printf "max_diff[%s] = %d\n" s !max_diff)
        !diffs ;
      Printf.printf "\n"
    in
    let (record_info, get_info) = new_table "info" in
    let (record_tree, get_tree) = new_table "tree" in
    let (record_commit, get_commit) = new_table "commit" in
    let rec iter step =
      match (input_value ic : op) with
      | exception End_of_file ->
          finish step ; step
      | exception exn ->
          finish step ; raise exn
      | op ->
          ( print_op step op ;
            match op with
            | Config (_mapsize, _readonly, _index_log_size, _root) ->
                ()
            | Info_v (_date, _author, _message) ->
                record_info ()
            | Tree_hash (num, _hash) ->
                get_tree num
            | Tree_empty ->
                record_tree ()
            | Tree_mem (num, _path, _result) ->
                get_tree num
            | Tree_mem_tree (num, _path, _result) ->
                get_tree num
            | Tree_remove (num, _path) ->
                record_tree (get_tree num)
            | Tree_find (num, _path, _result) ->
                get_tree num
            | Tree_find_tree (num, _path, _result) ->
                record_tree (get_tree num)
            | Tree_add (num, _path, _value) ->
                record_tree (get_tree num)
            | Tree_add_tree (num1, _path, num2) ->
                get_tree num2 ;
                record_tree (get_tree num1)
            | Tree_list (num, _path, _result) ->
                get_tree num
            | Commit_tree num ->
                record_tree (get_commit num)
            | Commit_info num ->
                get_commit num ; record_info ()
            | Commit_of_hash _hash ->
                record_commit ()
            | Commit_v (num_info, parents, num_tree, _result) ->
                get_info num_info ;
                List.iter get_commit parents ;
                get_tree num_tree ;
                record_commit ()
            | Commit_hash (num_commit, _hash) ->
                get_commit num_commit
            | Hash (_date, _message, num_parents, num_tree, _hash) ->
                List.iter get_commit num_parents ;
                get_tree num_tree ) ;
          iter (step + 1)
    in
    iter 0

  let ocr = ref None

  let rec record_op op =
    match !ocr with
    | Some oc ->
        output_value oc op ; flush oc
    | None -> (
      match op with
      | Config (_mapsize, _readonly, _index_log_size, root) ->
          let oc = open_out_bin (root ^ ".dump") in
          ocr := Some oc ;
          record_op op
      | _ ->
          assert false )

  let make_recorder () =
    let tree_counter = ref 0 in
    let record_tree op =
      let tree_number = !tree_counter in
      incr tree_counter ; record_op op ; tree_number
    in
    record_tree

  let record_tree = make_recorder ()

  let record_commit = make_recorder ()

  let record_info = make_recorder ()

  module Irmin = struct
    type config = B.Irmin.config

    module Info = struct
      type t = int * B.Irmin.Info.t

      let v ~date ~author message =
        let num = record_info (Info_v (date, author, message)) in
        let b = B.Irmin.Info.v ~date ~author message in
        (num, b)

      let message (_, b) = B.Irmin.Info.message b

      let author (_, b) = B.Irmin.Info.author b

      let date (_, b) = B.Irmin.Info.date b
    end

    let config ?mapsize ?readonly ?index_log_size root =
      let b_config = B.Irmin.config ?mapsize ?readonly ?index_log_size root in
      record_op (Config (mapsize, readonly, index_log_size, root)) ;
      b_config
  end

  module Store = struct
    type hash = Hash.t

    type tree = int * B.Store.tree

    module Tree = struct
      let mem (num, b_tree) path =
        B.Store.Tree.mem b_tree path
        >>= function
        | b_res ->
            record_op (Tree_mem (num, path, b_res)) ;
            Lwt.return b_res

      let mem_tree (num, b_tree) path =
        B.Store.Tree.mem_tree b_tree path
        >>= function
        | b_res ->
            record_op (Tree_mem_tree (num, path, b_res)) ;
            Lwt.return b_res

      let find (num, b_tree) path =
        B.Store.Tree.find b_tree path
        >>= function
        | b_res ->
            record_op
              (Tree_find
                 ( num,
                   path,
                   match b_res with
                   | None ->
                       None
                   | Some s ->
                       let len = String.length s in
                       Some
                         ( len,
                           if len > max_recorded_string_length then
                             String.sub s 0 (1 + max_recorded_string_length)
                           else s ) )) ;
            Lwt.return b_res

      let remove (num, b_tree) path =
        let num = record_tree (Tree_remove (num, path)) in
        B.Store.Tree.remove b_tree path
        >>= function b_res -> Lwt.return (num, b_res)

      let add (num, b_tree) path ?metadata value =
        assert (metadata = None) ;
        let num = record_tree (Tree_add (num, path, value)) in
        B.Store.Tree.add b_tree path value
        >>= function b_res -> Lwt.return (num, b_res)

      let add_tree (num1, b_tree) path (num2, b_value) =
        let num = record_tree (Tree_add_tree (num1, path, num2)) in
        B.Store.Tree.add_tree b_tree path b_value
        >>= function b_res -> Lwt.return (num, b_res)

      let find_tree (arg_num, b_tree) path =
        B.Store.Tree.find_tree b_tree path
        >>= function
        | b_res ->
            let res_num =
              record_tree
                (Tree_find_tree
                   ( arg_num,
                     path,
                     match b_res with None -> false | Some _ -> true ))
            in
            Lwt.return
              ( match b_res with
              | None ->
                  None
              | Some b_res ->
                  Some (res_num, b_res) )

      let list (num, b_tree) path =
        B.Store.Tree.list b_tree path
        >>= function
        | b_list ->
            record_op (Tree_list (num, path, b_list)) ;
            Lwt.return b_list
    end

    module Repo = struct
      type t = B.Store.Repo.t

      let v b_config = B.Store.Repo.v b_config
    end

    module Commit = struct
      type t = int * B.Store.Commit.t

      let of_hash b_repo hash =
        let num = record_commit (Commit_of_hash (Hash.to_context_hash hash)) in
        B.Store.Commit.of_hash b_repo hash
        >>= function
        | b_res ->
            Lwt.return
              ( match b_res with
              | None ->
                  None
              | Some b_res ->
                  Some (num, b_res) )

      let hash (num, b_t) =
        let b_res = B.Store.Commit.hash b_t in
        record_op (Commit_hash (num, Hash.to_context_hash b_res)) ;
        b_res

      let info (num, b_commit) =
        let num = record_info (Commit_info num) in
        let b_info = B.Store.Commit.info b_commit in
        (num, b_info)
    end

    module Branch = struct
      let set a_repo branch_name (_, a_commit) =
        B.Store.Branch.set a_repo branch_name a_commit

      let remove a_repo branch_name = B.Store.Branch.remove a_repo branch_name

      let master = B.Store.Branch.master
    end

    let repo_close b_repo = B.Store.repo_close b_repo

    let tree_empty () =
      let num = record_tree Tree_empty in
      (num, B.Store.tree_empty ())

    let tree_hash b_repo (num, b_tree) =
      B.Store.tree_hash b_repo b_tree
      >>= function
      | `Node hash ->
          record_op (Tree_hash (num, `Node (B.Hash.to_context_hash hash))) ;
          Lwt.return (`Node hash)
      | `Contents (hash, _) ->
          record_op
            (Tree_hash (num, `Contents (B.Hash.to_context_hash hash, ()))) ;
          Lwt.return (`Contents (hash, ()))

    let tree_clear ?depth (_num, b_tree) =
      B.Store.tree_clear ?depth b_tree ;
      ()

    let commit_tree (num, b_commit) =
      let num = record_tree (Commit_tree num) in
      B.Store.commit_tree b_commit
      >>= function b_tree -> Lwt.return (num, b_tree)

    let commit_v b_repo ~info:(num_info, b_info) ~parents (num_tree, b_tree) =
      let parents =
        List.map
          (function
            | `Commit (num_c, b_c) ->
                (num_c, `Commit b_c)
            | `Hash b_c ->
                (assert false, `Hash b_c)
            | _ ->
                assert false)
          parents
      in
      let (num_parents, b_parents) = List.split parents in
      B.Store.commit_v b_repo ~info:b_info ~parents:b_parents b_tree
      >>= function
      | b_res ->
          let num =
            record_commit
              (Commit_v
                 ( num_info,
                   num_parents,
                   num_tree,
                   Hash.to_context_hash (B.Store.Commit.hash b_res) ))
          in
          Lwt.return (num, b_res)
  end

  let hash ~time ?(message = "") ~parents ~tree =
    let (num_tree, b_tree) = tree in
    let (num_parents, b_parents) = List.split parents in
    let b = B.hash ~time ~message ~parents:b_parents ~tree:b_tree in
    record_op
      (Hash (Time.Protocol.to_seconds time, message, num_parents, num_tree, b)) ;
    b

  let gc = B.gc

  let revert = B.revert

  let clear_stats = B.clear_stats

  let print_stats = B.print_stats

  let storage_dir = B.storage_dir

  module MEMCACHE = struct
    type t = B.MEMCACHE.t

    let create repo f = B.MEMCACHE.create repo f

    let add_string batch string = B.MEMCACHE.add_string batch string

    let add_dir batch l =
      B.MEMCACHE.add_dir batch l
      >|= function None -> None | Some tree -> Some (-1, tree)
  end

  let fold_tree_path ?progress repo (_, tree) f =
    B.fold_tree_path ?progress repo tree f

  let raw_commit (_, info) (repo, parents, (_, tree)) =
    let (_, parents) = List.split parents in
    B.raw_commit info (repo, parents, tree) >|= fun c -> (-1, c)

  let context_parents ctxt (_, commit) = B.context_parents ctxt commit

  let set_context batch ~info:(_, info) ~parents (repo, (_, tree)) bh =
    B.set_context batch ~info ~parents (repo, tree) bh

  let compute_context_hash repo (_, tree) ~info:(_, info) ~parents_hashes
      data_hash =
    B.compute_context_hash repo tree ~info ~parents_hashes data_hash
    >|= fun (computed_context_hash, mock_parents, data_tree, commit_parents) ->
    let mock_parents = List.map (fun c -> (-1, c)) mock_parents in
    let commit_parents =
      List.map
        (function `Commit c -> `Commit (-1, c) | `Hash h -> `Hash h)
        commit_parents
    in
    (computed_context_hash, mock_parents, (-1, data_tree), commit_parents)
end
