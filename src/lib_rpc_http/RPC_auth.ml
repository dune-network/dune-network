(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let check_basic_auth ~users ~headers =
  let rec iter headers =
    match headers with
    | [] ->
        false
    | (name, value) :: headers -> (
      match name with
      | "authorization" -> (
        match String.split ' ' value with
        | ["Basic"; passwd] -> (
          match Base64.decode_exn passwd with
          | exception _ ->
              false
          | value -> (
            match String.split ':' value with
            | [user; passwd] -> (
              match String.Map.find user users with
              | exception Not_found ->
                  false
              | p ->
                  Compare.String.(p = passwd) )
            | _ ->
                false ) )
        | _ ->
            false (* Basic ZmFicmljZTpmYWI= *) )
      | _ ->
          iter headers )
  in
  iter headers
