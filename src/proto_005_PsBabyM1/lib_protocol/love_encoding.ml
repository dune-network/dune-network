(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Data_encoding

module Type = struct
  let encoding =
    splitted
      ~json:Love_json_encoding.Type.encoding
      ~binary:Love_binary_encoding.Type.encoding
end

module Value = struct
  let encoding =
    splitted
      ~json:Love_json_encoding.Value.encoding
      ~binary:Love_binary_encoding.Value.encoding

  let live_structure_encoding =
    splitted
      ~json:Love_json_encoding.Value.live_structure_encoding
      ~binary:Love_binary_encoding.Value.live_structure_encoding

  let live_contract_encoding =
    splitted
      ~json:Love_json_encoding.Value.live_contract_encoding
      ~binary:Love_binary_encoding.Value.live_contract_encoding

  let fee_code_encoding =
    splitted
      ~json:Love_json_encoding.Value.fee_code_encoding
      ~binary:Love_binary_encoding.Value.fee_code_encoding
end

module Ast = struct
  let const_encoding =
    splitted
      ~json:Love_json_encoding.Ast.const_encoding
      ~binary:Love_binary_encoding.Ast.const_encoding
  let exp_encoding =
    splitted
      ~json:Love_json_encoding.Ast.exp_encoding
      ~binary:Love_binary_encoding.Ast.exp_encoding
  let structure_encoding =
    splitted
      ~json:Love_json_encoding.Ast.structure_encoding
      ~binary:Love_binary_encoding.Ast.structure_encoding
  let top_contract_encoding =
    splitted
      ~json:Love_json_encoding.Ast.top_contract_encoding
      ~binary:Love_binary_encoding.Ast.top_contract_encoding
end

module RuntimeAst = struct
  let const_encoding =
    splitted
      ~json:Love_json_encoding.RuntimeAst.const_encoding
      ~binary:Love_binary_encoding.RuntimeAst.const_encoding
  let exp_encoding =
    splitted
      ~json:Love_json_encoding.RuntimeAst.exp_encoding
      ~binary:Love_binary_encoding.RuntimeAst.exp_encoding
  let structure_encoding =
    splitted
      ~json:Love_json_encoding.RuntimeAst.structure_encoding
      ~binary:Love_binary_encoding.RuntimeAst.structure_encoding
  let top_contract_encoding =
    splitted
      ~json:Love_json_encoding.RuntimeAst.top_contract_encoding
      ~binary:Love_binary_encoding.RuntimeAst.top_contract_encoding
end
