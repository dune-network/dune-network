(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module M = struct

  open Alpha_context
  module Repr = Lang1_script_repr

  open Repr

  type execution_result = {
    ctxt : context ;
    storage : const ;
    result : const option ;
    big_map_diff : Contract.big_map_diff option ;
    operations : packed_internal_operation list ;
  }

  type fee_execution_result = {
    ctxt : context ;
    max_fee : Tez.t ;
    max_storage : Z.t ;
  }

  type val_execution_result = {
    ctxt : context;
    result : Repr.const;
  }

  let interp ctxt _mode _step_constants state instr = match instr with
    | NOOP -> return (ctxt, state)

  let execute ctxt mode step_constants ~code ~storage ~entrypoint:_ ~parameter ~apply:_ =
    interp ctxt mode step_constants (parameter, storage) code
    >>=? fun (ctxt, (_result_state, _storage)) ->
    (*  Dune_storage.update_contract_storage ctxt self
        (Lang1_repr.Const storage) >>=? fun ctxt -> *)
    return { ctxt; storage; result = Some VOID;
             big_map_diff = None; operations = [] }

  let normalize_script ctxt _self code storage =
    return ((code, storage, None), ctxt)

  let denormalize_script ctxt code storage _fee_code =
    return ((code, storage), ctxt)

  let typecheck_code ctxt _code =
    return (ctxt)

  let typecheck_data ctxt _data _type =
    return (ctxt)

  let typecheck ctxt _step_constants ~code ~storage =
    return ((code, storage, None), None, ctxt)

  let execute_fee_script ctxt step_constants ~fee_code ~storage ~entrypoint:_ ~parameter =
    interp ctxt Script_ir_translator.Readable step_constants (parameter, storage) fee_code
    >>=? fun (ctxt, _) ->
    return { ctxt ; max_fee = Tez.zero ; max_storage = Z.zero }

  let execute_value ctxt ~self:_ ~code:_ ~storage:_ ~val_name:_ ~parameter:_ =
    return {ctxt ; result = VOID}

  let get_entrypoint ctxt ~code:_ ~entrypoint:_ =
    return (ctxt, VOID)
  (* Disable after revision 1 *)

  let list_entrypoints ctxt ~code:_ =
    return (ctxt, [])

  let pack_data ctxt ~typ:_ ~data:_ =
    return (ctxt, MBytes.of_string "")

  let unpack_data ctxt ~typ:_ ~data:_ =
    return (ctxt, VOID)

  let hash_data ctxt ~code:_ =
    return (ctxt, Script_expr_hash.zero)

  let to_michelson_const _ctxt _k = failwith "No Lang1"

  let from_michelson_const _ctxt _k = failwith "No Lang1"
  (* Disable after revision 1 *)

  let typecheck ctxt step_constants ~code ~storage =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      typecheck ctxt step_constants ~code ~storage

  let execute ctxt mode step_constants ~code ~storage ~entrypoint ~parameter ~apply =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      execute ctxt mode step_constants ~code ~storage ~entrypoint ~parameter ~apply

  let execute_fee_script ctxt step_constants ~fee_code ~storage ~entrypoint ~parameter =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      execute_fee_script ctxt step_constants ~fee_code ~storage ~entrypoint ~parameter

  let execute_value ctxt ~self ~code ~storage ~val_name ~parameter =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      execute_value ctxt ~self ~code ~storage ~val_name ~parameter

  let get_entrypoint ctxt ~code ~entrypoint =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      get_entrypoint ctxt ~code ~entrypoint

  let list_entrypoints ctxt ~code =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      list_entrypoints ctxt ~code

  let pack_data ctxt ~typ ~data =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      pack_data ctxt ~typ ~data

  let unpack_data ctxt ~typ ~data =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      unpack_data ctxt ~typ ~data


  let hash_data ctxt k =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      hash_data ctxt k
end

include M

let () =
  if false then
    let module TEST = ( M : Dune_script_interpreter_sig.S ) in
    ()
