(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

include Script_int_repr

type t = n num

type tez = t

type error +=
  | Tez_conversion_overflow of t (* `Temporary *)
  | Tez_subtraction_negative of t * t

(* `Temporary *)

let equal x y = Compare.Int.equal (compare x y) 0

let ( = ) = equal

let ( <> ) x y = not (equal x y)

let ( < ) x y = Compare.Int.( < ) (compare x y) 0

let ( <= ) x y = Compare.Int.( <= ) (compare x y) 0

let ( >= ) x y = Compare.Int.( >= ) (compare x y) 0

let ( > ) x y = Compare.Int.( > ) (compare x y) 0

let max x y = if x >= y then x else y

let min x y = if x <= y then x else y

let ( - ) x y = sub x y |> is_nat

let ( -? ) x y =
  match x - y with
  | None ->
      error (Tez_subtraction_negative (x, y))
  | Some x ->
      ok x

let ( + ) = add_n

let ( * ) = mul_n

let ( / ) (x : t) (y : t) : (n num * t) option =
  match ediv_n x y with None -> None | Some (d, r) -> Some (d, r)

let div_nat (x : t) (y : n num) : (t * t) option = ediv_n x y

let of_int z =
  assert (Compare.Int.(z >= 0)) ;
  Z.of_int z |> of_zint |> abs

let zero = zero_n

let one_mutez = of_int 1

let one_cent = one_mutez * of_int 10_000

let fifty_cents = one_cent * of_int 50

(* 1 tez = 100 cents = 1_000_000 mutez *)
let one = one_cent * of_int 100

let ten = one * of_int 10

let hundred = one * of_int 100

let of_mutez x = of_zint x |> is_nat

let of_mutez_exn x =
  match of_mutez x with None -> invalid_arg "ZTez.of_mutex_exn" | Some x -> x

let of_mutez_nat (x : n num) : t = x

let to_mutez = to_zint

let to_tez x =
  try x |> to_zint |> Z.to_int64 |> Tez_repr.of_mutez_exn |> ok
  with Z.Overflow -> error (Tez_conversion_overflow x)

let of_tez x =
  let z = x |> Tez_repr.to_mutez |> Z.of_int64 |> of_zint in
  match is_nat z with None -> assert false | Some n -> n

let encoding =
  let open Data_encoding in
  conv to_mutez (Json.wrap_error of_mutez_exn) n

let of_string s : t option =
  let triplets = function
    | hd :: tl ->
        let len = String.length hd in
        Compare.Int.(
          len <= 3 && len > 0 && List.for_all (fun s -> String.length s = 3) tl)
    | [] ->
        false
  in
  let integers s = triplets (String.split_on_char ',' s) in
  let decimals s =
    let l = String.split_on_char ',' s in
    if Compare.Int.(List.length l > 2) then false else triplets (List.rev l)
  in
  let parse left right =
    let remove_commas s = String.concat "" (String.split_on_char ',' s) in
    let pad_to_six s =
      let len = String.length s in
      String.init 6 (fun i -> if Compare.Int.(i < len) then s.[i] else '0')
    in
    try
      of_mutez
      @@ Z.of_string (remove_commas left ^ pad_to_six (remove_commas right))
    with _ -> None
  in
  match String.split_on_char '.' s with
  | [left; right] ->
      if String.contains s ',' then
        if integers left && decimals right then parse left right else None
      else if
        Compare.Int.(String.length right > 0)
        && Compare.Int.(String.length right <= 6)
      then parse left right
      else None
  | [left] ->
      if (not (String.contains s ',')) || integers left then parse left ""
      else None
  | _ ->
      None

let pp ppf amount =
  let mult_int = of_int 1_000_000 in
  let i x = Z.to_int (to_zint x) in
  let ( // ) x y =
    match x / y with None -> assert false | Some (d, r) -> (d, r)
  in
  let rec left ppf amount =
    let (d, r) = amount // of_int 1000 in
    if d > zero then Format.fprintf ppf "%a%03d" left d (i r)
    else Format.fprintf ppf "%d" (i r)
  in
  let right ppf amount =
    let triplet ppf v =
      if snd @@ (v // of_int 10) > zero then Format.fprintf ppf "%03d" (i v)
      else if snd @@ (v // of_int 100) > zero then
        Format.fprintf ppf "%02d" (v // of_int 10 |> fst |> i)
      else Format.fprintf ppf "%d" (v // of_int 100 |> fst |> i)
    in
    let (hi, lo) = amount // of_int 1000 in
    if lo = zero then Format.fprintf ppf "%a" triplet hi
    else Format.fprintf ppf "%03d%a" (i hi) triplet lo
  in
  let (ints, decs) = amount // mult_int in
  Format.fprintf ppf "%a" left ints ;
  if decs > zero then Format.fprintf ppf ".%a" right decs

let to_string t = Format.asprintf "%a" pp t

let () =
  let open Data_encoding in
  register_error_kind
    `Temporary
    ~id:(Tez_repr.id ^ ".conversion_overflow")
    ~title:("Overflowing " ^ Tez_repr.id ^ " conversion")
    ~pp:(fun ppf x ->
      Format.fprintf ppf "Overflowing conversion of %a %s" pp x Tez_repr.id)
    ~description:
      ( "A convertion of a " ^ Tez_repr.id
      ^ " to external representation (int64) overflowed" )
    (obj1 (req "amount" encoding))
    (function Tez_conversion_overflow x -> Some x | _ -> None)
    (fun x -> Tez_conversion_overflow x) ;
  register_error_kind
    `Temporary
    ~id:(Tez_repr.id ^ ".subtraction_negative")
    ~title:("Negative " ^ Tez_repr.id ^ " subtraction")
    ~pp:(fun ppf (x, y) ->
      Format.fprintf
        ppf
        "Negative subtraction of %a %s and %a %s"
        pp
        x
        Tez_repr.id
        pp
        y
        Tez_repr.id)
    ~description:
      ("A subtraction of two " ^ Tez_repr.id ^ " led to a negative amount")
    (obj2 (req "arg1" encoding) (req "arg2" encoding))
    (function Tez_subtraction_negative (x, y) -> Some (x, y) | _ -> None)
    (fun (x, y) -> Tez_subtraction_negative (x, y))
