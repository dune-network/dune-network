(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type t

type tez = t

type error +=
  | Tez_conversion_overflow of t (* `Temporary *)
  | Tez_subtraction_negative of t * t

(* `Temporary *)

val ( - ) : t -> t -> t option

val ( -? ) : t -> t -> t tzresult

val ( + ) : t -> t -> t

val ( * ) : t -> t -> t

val ( / ) : t -> t -> (Script_int_repr.n Script_int_repr.num * t) option

val div_nat : t -> Script_int_repr.n Script_int_repr.num -> (t * t) option

val zero : t

val one_mutez : t

val one_cent : t

val fifty_cents : t

val one : t

val ten : t

val hundred : t

val to_mutez : t -> Z.t

val of_mutez : Z.t -> t option

val of_mutez_exn : Z.t -> t

val of_mutez_nat : Script_int_repr.n Script_int_repr.num -> t

val to_tez : t -> Tez_repr.t tzresult

val of_tez : Tez_repr.t -> t

val encoding : t Data_encoding.t

val to_zint : t -> Z.t

include Compare.S with type t := t

val pp : Format.formatter -> t -> unit

val of_string : string -> t option

val to_string : t -> string
