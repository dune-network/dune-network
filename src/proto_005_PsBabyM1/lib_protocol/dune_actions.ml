(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module StringMap = Map.Make(String)

module PROTOCOL = struct

  let actions = ref StringMap.empty

  let perform_action ctxt name =
    match StringMap.find_opt name !actions with
    | None ->
        Dune_misc.failwith "Undefined protocol action %S" name
    | Some ( revision, action ) ->
        if Dune_storage.has_protocol_revision ctxt revision then
          action ctxt
        else
          Dune_misc.failwith "perform_action %s : current revision < %d"
            name revision

  let register_action name revision f =
    if StringMap.mem name !actions then
      Dune_debug.printf ~n:0
        "Warning: protocol action %S registered twice" name;
    actions := StringMap.add name ( revision, f ) !actions

  let is_action at_revision action =
    match StringMap.find_opt action !actions with
    | None -> false
    | Some ( revision, _ ) ->
        (* do not allow a message containing an action that may not be
           known by all nodes in the network, because they are not
           aware of the given revision. *)
        Compare.Int.(>=) at_revision revision

  (* Protocol actions *)
end

let change_delegations ctxt contract delegation =
  begin
    match Contract_repr.is_implicit contract with
    | None ->
        Dune_misc.failwith "change_delegations only available for bakers"
    | Some delegate ->
        return delegate
  end >>=? fun delegate ->
  begin
    match delegation with
    | None -> return ctxt
    | Some baker ->
        Delegate_storage.registered ctxt baker >>=? fun is_baker ->
        if is_baker then
          return ctxt
        else
          Dune_misc.failwith
            "move_delegations: target must be an active baker"
  end >>=? fun ctxt ->
  Delegate_storage.delegated_contracts ctxt delegate >>= fun contracts ->
  let n = ref 0 in
  fold_left_s (fun ctxt source ->
      incr n;
      if not ( Dune_storage.has_protocol_revision ctxt 3 ) ||
         Contract_repr.( source <> contract ) then
        Delegate_storage.set ctxt source delegation
      else
        return ctxt
    ) ctxt contracts

module ACCOUNTS = struct

  let actions = ref StringMap.empty

  let perform_action ctxt balance_updates action amount contracts =
    match StringMap.find_opt action !actions with
    | None ->
        Dune_misc.failwith "Undefined accounts action %S" action
    | Some action ->
        fold_left_s
          (fun (ctxt, balances) contract ->
             action ctxt balances amount contract)
          (ctxt, balance_updates) contracts

  let register_action name f =
    if StringMap.mem name !actions then
      Dune_debug.printf ~n:0 "Warning: accounts action %S registered twice" name;
    actions := StringMap.add name f !actions

  let register_simple_action name f =
    register_action name (fun ctxt balances _amount contract ->
        f ctxt contract >>=? fun ctxt -> return (ctxt, balances)
      )

  (* Accounts actions, using Dictator's Manage_accounts *)

end


module ACCOUNT = struct

  let actions = ref StringMap.empty

  let perform_action ctxt balance_updates action contract arg =
    match StringMap.find_opt action !actions with
    | None ->
        Dune_misc.failwith "Undefined account action %S" action
    | Some action ->
        action ctxt balance_updates contract arg

  let register_action name f =
    if StringMap.mem name !actions then
      Dune_debug.printf ~n:0 "Warning: account action %S registered twice" name;
    actions := StringMap.add name f !actions

  open Love_value.Value

  let register_unit_action name f =
    register_action name (fun ctxt balances contract arg ->
        match arg with
        | VUnit -> f ctxt contract >>=? fun ctxt -> return (ctxt, balances)
        | _ -> Dune_misc.failwith "unexpected arg for %s" name)


end

let initial_check_revision = 5

let () =
  PROTOCOL.register_action "freeze_undelegated_accounts" initial_check_revision
    (fun ctxt ->
       Storage.Contract.fold ctxt ~init:(Ok ctxt)
         ~f:(fun contract ctxt ->
             Lwt.return ctxt >>=? fun ctxt ->
             match Contract_repr.is_implicit contract with
             | None -> return ctxt
             | Some _key_hash ->
                 Delegate_storage.get ctxt contract >>=? function
                 | Some _ -> return ctxt
                 | None ->
                     Delegate_storage.freeze_account ctxt contract
           )
    );

  PROTOCOL.register_action "burn_all_commitments" initial_check_revision
    (fun ctxt ->
       Storage.Commitments.clear ctxt >>= return
    );

  PROTOCOL.register_action "activate_burn_legacy" initial_check_revision
    (fun ctxt ->
       let cycle = (Raw_context.current_level ctxt).cycle in
       (* No need to select the next cycle, this action takes place on cycle
          end, so it can happen on end of the current cycle. *)
       Storage.Cycle.Next_deflate.init_set ctxt cycle
       >>= fun ctxt ->
       Storage.Cycle.Unburn.get_option ctxt >>=? function
       | None ->
           Storage.Cycle.Unburn.init_set ctxt 1 >>= return
       | Some _ -> return ctxt
    );

  PROTOCOL.register_action "deactivate_burn_legacy" initial_check_revision
    (fun ctxt ->
       Storage.Cycle.Next_deflate.remove ctxt
       >>= fun ctxt -> return ctxt
    );

  PROTOCOL.register_action "recompute_staking_rights" initial_check_revision
    (fun ctxt ->
       if Dune_storage.has_protocol_revision ctxt 5  then begin
         let module ContractSet = Set.Make (Contract_repr) in
         let tokens_per_roll = Constants_storage.tokens_per_roll ctxt in

         let fixed_bakers = ref ContractSet.empty in

         let fix_baker ctxt pkh =
           let contract = Contract_repr.implicit_contract pkh in
           if not ( ContractSet.mem contract !fixed_bakers ) then begin

             Storage.Contract.Balance.get ctxt contract
             >>= fun self_staking_balance ->
             Storage.Contract.Frozen_deposits.fold
               (ctxt, contract) ~init:self_staking_balance
               ~f:(fun _cycle amount acc ->
                   Lwt.return acc >>=? fun acc ->
                   Lwt.return (Tez_repr.(acc +? amount)))
             >>= fun self_staking_balance ->
             Storage.Contract.Frozen_fees.fold
               (ctxt, contract) ~init:self_staking_balance
               ~f:(fun _cycle amount acc ->
                   Lwt.return acc >>=? fun acc ->
                   Lwt.return (Tez_repr.(acc +? amount)))
             >>=? fun self_staking_balance ->

             Storage.Contract.Delegated.fold (ctxt, contract)
               ~init:(Ok self_staking_balance)
               ~f:(fun contract staking_balance ->
                   Lwt.return staking_balance >>=? fun staking_balance ->
                   Storage.Contract.Balance.get_option ctxt contract
                   >>=? function
                   | None -> return staking_balance
                   | Some balance ->
                       Lwt.return Tez_repr.(staking_balance +? balance))
             >>=? function new_staking_balance ->

               Delegate_storage.staking_balance ctxt pkh >>=?
               fun old_staking_balance ->

               begin
                 if Tez_repr.( new_staking_balance > old_staking_balance) then
                   begin
                     Dune_debug.printf ~n:1
                       "Contract %s with corrected %Ld > old %Ld"
                       (Signature.Public_key_hash.to_b58check pkh)
                       (Tez_repr.to_mutez new_staking_balance)
                       (Tez_repr.to_mutez old_staking_balance) ;
                     Lwt.return
                       Tez_repr.( new_staking_balance -? old_staking_balance )
                     >>=? fun amount ->
                     Roll_storage.Contract.add_amount ctxt contract amount
                   end
                 else
                 if Tez_repr.( new_staking_balance < old_staking_balance) then
                   begin
                     Dune_debug.printf ~n:0
                       "Contract %s with corrected %Ld < old %Ld"
                       (Signature.Public_key_hash.to_b58check pkh)
                       (Tez_repr.to_mutez new_staking_balance)
                       (Tez_repr.to_mutez old_staking_balance) ;
                     Lwt.return
                       Tez_repr.( old_staking_balance -? new_staking_balance )
                     >>=? fun amount ->
                     Roll_storage.Contract.remove_amount ctxt contract amount
                   end
                 else begin
                   return ctxt
                 end
               end
               >>=? fun ctxt ->
               Roll_storage.Delegate.is_inactive ctxt pkh >>=? fun inactive ->
               Roll_storage.Delegate.count_rolls ctxt pkh >>=? fun counted_rolls
               ->
               begin
                 if inactive then begin
                   assert Compare.Int.( counted_rolls = 0 );
                   Roll_storage.Delegate.get_nrolls ctxt pkh >>=?
                   fun (_nrolls, nrolls_avail) ->
                   begin
                     match nrolls_avail with
                     | None -> ()
                     | Some nrolls ->
                         Dune_debug.printf ~n:1
                           "inactive baker with %d rolls" nrolls
                   end;
                   Storage.Contract.Delegate_nrolls.init_set ctxt contract 0
                   >>= fun ctxt ->
                   Storage.Roll.Delegate_change.set ctxt pkh
                     new_staking_balance
                 end
                 else begin
                   let new_staking_balance_mutez =
                     Tez_repr.to_mutez new_staking_balance in
                   let expected_number_of_rolls =
                     Int64.div new_staking_balance_mutez
                       ( Tez_repr.to_mutez tokens_per_roll ) in
                   Lwt.return Tez_repr.(tokens_per_roll *?
                                        expected_number_of_rolls)
                   >>=? fun rolls_balance ->
                   Roll_storage.get_change ctxt pkh >>=? fun change ->
                   Lwt.return Tez_repr.(rolls_balance +? change)
                   >>=? fun expected_staking_balance ->
                   if Tez_repr.( expected_staking_balance <> new_staking_balance )
                   then
                     Dune_debug.printf ~n:1
                       "Contract %s has expected %Ld <> computed %Ld"
                       (Signature.Public_key_hash.to_b58check pkh)
                       (Tez_repr.to_mutez expected_staking_balance)
                       (Tez_repr.to_mutez new_staking_balance) ;
                   return ctxt
                 end
               end >>=? fun ctxt ->
               fixed_bakers := ContractSet.add contract !fixed_bakers;
               return ctxt
           end
           else

             return ctxt
         in
         Storage.Contract.fold ctxt ~init:(Ok ctxt)
           ~f:(fun contract ctxt ->
               Lwt.return ctxt >>=? fun ctxt ->
               match Contract_repr.is_implicit contract with
               | None -> return ctxt
               | Some pkh ->

                   Storage.Contract.Delegate.get_option ctxt contract
                   >>=? function
                   | None -> return ctxt

                   | Some delegate_pkh ->

                       if Signature.Public_key_hash.( delegate_pkh <> pkh) then

                         (* delegated *)
                         let delegate_contract =
                           Contract_repr.implicit_contract delegate_pkh in
                         Storage.Contract.Delegated.mem
                           (ctxt, delegate_contract ) contract >>= function
                         | true -> return ctxt
                         | false ->
                             Dune_debug.printf ~n:1
                               "Contract %s wrongly delegated to %s"
                               (Contract_repr.to_b58check contract)
                               (Signature.Public_key_hash.to_b58check
                                  delegate_pkh);

                             Storage.Contract.Delegate.remove ctxt contract
                             >>= fun ctxt -> return ctxt
                       else

                         (* baker *)
                         begin
                           Storage.Contract.Delegated.mem
                             (ctxt, contract ) contract >>= function
                           | false -> return ctxt
                           | true ->
                               Storage.Contract.Delegated.del
                                 (ctxt, contract) contract >>= return
                         end >>=? fun ctxt ->
                         fix_baker ctxt delegate_pkh
             ) >>=? fun ctxt ->
         return ctxt
       end else
         failwith "Invalid actions for revision < 5"
    );


  (* ACCOUNT actions *)

  ACCOUNT.register_unit_action "clear_delegations"
    (fun ctxt contract ->
       change_delegations ctxt contract None );

  ACCOUNT.register_action "move_delegations"
    (fun ctxt balances contract arg ->
       match arg with
       | VAddress delegate ->
           begin
             match Contract_repr.is_implicit delegate with
             | None ->
                 Dune_misc.failwith
                   "move_delegations: target must be a baker pkh"
             | Some delegate -> return delegate
           end >>=? fun delegate ->
           change_delegations ctxt contract (Some delegate) >>=? fun ctxt ->
           return ( ctxt, balances )
       | _ ->
           Dune_misc.failwith
             "move_delegations: arg must be destination baker"
    );

  ACCOUNT.register_action "recover_account"
    (fun ctxt balances source arg ->
       match arg with
       | VAddress destination ->
           begin
             Contract_storage.get_recovery ctxt destination >>=? function
             | Some recovery when Contract_repr.( recovery = source ) ->
                 begin
                   (* undelegate to be able to clear the balance *)
                   begin
                     match Contract_repr.is_implicit destination with
                     | None ->
                         Dune_misc.failwith
                           "recover_account: recovered account cannot be a KT1"
                     | Some pkh -> return pkh
                   end >>=? fun destination_pkh ->
                   begin
                     Delegate_storage.registered ctxt destination_pkh
                     >>=? fun is_baker ->
                     if is_baker then
                       return ctxt
                     else
                       Delegate_storage.set ctxt destination None
                   end >>=? fun ctxt ->
                   (* transfer the balance *)
                   Contract_storage.get_balance ctxt destination
                   >>=? fun balance ->
                   Contract_storage.spend ctxt destination balance >>=?
                   fun ctxt ->
                   Contract_storage.credit ctxt destination balance >>=?
                   fun ctxt ->
                   return (ctxt, balances)
                 end
             | Some _ ->
                 Dune_misc.failwith
                   "recover_account: recovered account has another recovery account"
             | None ->
                 Dune_misc.failwith
                   "recover_account: recovered account has no recovery account"
           end
       | _ ->
           Dune_misc.failwith
             "recover_account: arg must be recovered account"
    );



  ACCOUNTS.register_simple_action "unfreeze"
    (fun ctxt contract ->
       Delegate_storage.unfreeze_account ctxt contract
    ) ;


  ACCOUNTS.register_simple_action "undelegate" (fun ctxt contract ->
      change_delegations ctxt contract None ) ;


  ACCOUNTS.register_simple_action "add_allowed_baker"
    (fun ctxt contract ->
       Contract_storage.change_allowed_baker ctxt contract true
    ) ;

  ACCOUNTS.register_simple_action "remove_allowed_baker"
    (fun ctxt contract ->
       Contract_storage.change_allowed_baker ctxt contract false
    ) ;

  ACCOUNTS.register_simple_action "add_superadmin"
    (fun ctxt contract ->
       Contract_storage.change_superadmin ctxt contract true >>= return
    ) ;

  ACCOUNTS.register_simple_action "remove_superadmin"
    (fun ctxt contract ->
       Contract_storage.change_superadmin ctxt contract false >>= return
    ) ;

  ACCOUNTS.register_action "kyc_accounts"
    (fun ctxt balances amount contract ->
       if Dune_storage.has_protocol_revision ctxt 3 then
         Contract_storage.change_kyc ctxt contract
           ( Tez_repr.to_mutez amount |> Int64.to_int ) >>=?
         fun ctxt -> return (ctxt, balances)
       else
         Dune_misc.failwith "set_kyc: not available in revision < 3"
    );


  ()
