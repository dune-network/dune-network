(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* [forge_manager_operation cs pos op] forges the binary version of [op] and
   stores it in [cs] at pos [pos] and returns the new position.*)
val forge_manager_operation :
  Cstruct.t -> int -> Forge_types.manager_operation -> int

(* [with_cstruct f] creates a Cstruct.t of length 10_000 by default,
   call the function [f cs pos] to fill it (returning the new
   position), and return the resulting sub-part *)
val with_cstruct : ?length:int -> (Cstruct.t -> int -> int) -> Cstruct.t

val to_string : Cstruct.t -> string

val to_hexa : Cstruct.t -> string

(* Simple fillers *)
val set_uint8 : Cstruct.t -> int -> int -> int

val set_uint32 : Cstruct.t -> int -> int32 -> int

val set_bytes : Cstruct.t -> int -> bytes -> int

val set_string : Cstruct.t -> int -> string -> int
