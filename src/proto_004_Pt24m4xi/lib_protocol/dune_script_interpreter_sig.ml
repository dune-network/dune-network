(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context

module type S = sig

  module Repr : Dune_script_sig.S

  type execution_result = {
    ctxt : context ;
    storage : Repr.const ;
    result : Repr.const ;
    operations : packed_internal_operation list ;
  }

  type fee_execution_result = {
    ctxt : context ;
    max_fee : Tez.t ;
    max_storage : Z.t ;
  }

  val typecheck :
    Alpha_context.t -> code:Repr.code -> storage:Repr.const ->
    ((Repr.code (* code *) * Repr.const (* storage *) * Repr.code option (* fee code *)) * Alpha_context.t) tzresult Lwt.t

  val execute:
    Alpha_context.t ->
    Script_ir_translator.unparsing_mode ->
    source: Contract.t ->
    payer: Contract.t ->
    self: Contract.t ->
    code: Repr.code ->
    storage: Repr.const ->
    parameter: Repr.const ->
    collect_call: bool ->
    amount: Tez.t ->
    apply:(
      Alpha_context.t -> 'kind manager_operation ->
      (context *
       'kind Apply_results.successful_manager_operation_result *
       packed_internal_operation list) tzresult Lwt.t
    ) ->
    execution_result tzresult Lwt.t

  val execute_fee_script:
    Alpha_context.t ->
    source: Contract.t ->
    payer: Contract.t ->
    self: Contract.t ->
    fee_code: Repr.code ->
    storage: Repr.const ->
    parameter: Repr.const ->
    amount: Tez.t ->
    fee_execution_result tzresult Lwt.t

end
