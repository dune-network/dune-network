(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(*
let command root =
  let rec iter root path =
    let files = Sys.readdir root in
    Array.iter (fun file ->
        let filename = Filename.concat root file in
        let pathname = Filename.concat path file in
        if Sys.is_directory filename then
          iter filename pathname
        else
          let content = Dun_misc.FILE.read filename in
          Printf.printf "  %S, %S ;\n" pathname content
      ) files
  in
  Printf.printf "let files = [\n";
  iter root "";
  Printf.printf "\n]"

*)

let command files =
  match files with
  | [] ->
      Printf.eprintf "Error: ix embed ROOT FILES\n%!" ;
      exit 2
  | root :: files ->
      Printf.printf "let files = [\n" ;
      let file_of_filename file =
        let rec iter path =
          match path with
          | dir :: path ->
              if dir = root then String.concat "/" path else iter path
          | [] ->
              Printf.eprintf
                "Error: %s\n  All files should start with www/root/\n%!"
                file ;
              exit 2
        in
        iter (Dune_config.Dune_std.STRING.split file '/')
      in
      List.iter
        (fun file ->
          let content = Dune_config.Dune_std.FILE.read file in
          Printf.printf "  %S, %S ;\n" (file_of_filename file) content)
        files ;
      Printf.printf "\n]"
