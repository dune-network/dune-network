(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_types

let command c ~lazy_flag args =
  let log = Ix_cmd_bake.node_tool_log c "node" in
  let node_pid =
    let (cmd, args) = Ix_cmd_run.command_args c args in
    Ix_common.daemon ~set_bindir c ~log ~cmd ~args
  in
  Printf.eprintf "Node starting with PID %d...\n%!" node_pid ;
  Printf.eprintf "  Check the log file with 'tail -f %s'\n%!" log ;
  Printf.eprintf "Waiting 2 seconds before activation...\n%!" ;
  Unix.sleep 2 ;
  ( match c.current_switch with
  | "mainnet" | "testnet" ->
      ()
  | _ ->
      let (cmd, args) = Ix_cmd_activate.command_args c args in
      Ix_common.call ~set_bindir c ~cmd ~args ) ;
  Ix_cmd_bake.command c [] ~one_flag:false ~lazy_flag ;
  Ix_common.waitpid node_pid

let ezcmd c =
  let lazy_flag = ref false in
  let anon_args = ref [] in
  Ix_common.ezcmd
    "start"
    ~args:
      Ezcmd.Modules.
        [ ( ["lazy"],
            Arg.Set lazy_flag,
            Ezcmd.info "Work lazily, adding a block only when needed" ) ]
    ~all:(fun args -> anon_args := args)
    ~action:(fun () -> command c !anon_args ~lazy_flag:!lazy_flag)
    ~doc:"Start a network node, activate the network and bake"
