(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_types

let command c =
  Printf.printf "Available switches:\n" ;
  let files = Sys.readdir Ix_common.duneswitches |> Array.to_list in
  let files = "mainnet" :: "testnet" :: files in
  let files = List.sort_uniq compare files |> Array.of_list in
  Array.iter
    (fun file ->
      if
        file = "mainnet" || file = "testnet"
        || Sys.file_exists (Ix_common.switch_file file)
      then
        if file = c.current_switch then
          Printf.printf
            "  %s (current: %s.%s)\n"
            file
            c.current_node.node_name
            c.current_client.client_name
        else Printf.printf "  %s\n" file)
    files ;
  Printf.printf "%!" ;
  ()

let ezcmd c =
  Ix_common.ezcmd "list" ~action:(fun () -> command c) ~doc:"List all networks"
