(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Config.TYPES

let identities_dir = "identities"

let identity_file n = Printf.sprintf "identity%d.json" n

let node_dir n = Printf.sprintf "node%d" n

let p2p_port i = Config.c.p2p_port + i

let rpc_port i = Config.c.rpc_port + i

let force_create = ref false

let started = ref None

let rec check_file file exists n =
  if n > 0 && not (Sys.file_exists file = exists) then
    let (_, _, _) = Unix.select [] [] [] 0.1 in
    check_file file exists (n - 1)

let rec stop_node dir =
  let lock_file = Filename.concat dir "lock" in
  let s = FileString.read_file lock_file in
  let pid = int_of_string s in
  command "kill" (Printf.sprintf "%d" pid) ;
  check_file lock_file false 10

and command cmd args =
  let cmd = cmd ^ " " ^ args in
  Printf.eprintf "Running: %s\n%!" cmd ;
  let ret = Sys.command cmd in
  if ret <> 0 then abort (Printf.sprintf "Command failed with status %d." ret)

and abort s =
  Printf.eprintf "Error: %s\nAborting.\n%!" s ;
  (match !started with None -> () | Some dir -> stop_node dir) ;
  exit 2

let command cmd fmt = Printf.kprintf (command cmd) fmt

let abort fmt = Printf.kprintf abort fmt

let stop_node dir =
  stop_node dir ;
  let lock_file = Filename.concat dir "lock" in
  if Sys.file_exists lock_file then abort "Could not kill node from %s" dir

let maybe_stop_node dir =
  let lock_file = Filename.concat dir "lock" in
  if Sys.file_exists lock_file then stop_node dir

let maybe_gen_file dir file_name file_content =
  let file = Filename.concat dir file_name in
  if !force_create || not (Sys.file_exists file) then (
    FileString.write_file file file_content ;
    Printf.eprintf "File %S created.\n%!" file )

let tezos_node fmt =
  command (Filename.concat Config.c.src_directory "dune-node") fmt

let tezos_client dir i fmt =
  command
    (Printf.sprintf
       "%s -base-dir %s/client -addr 127.0.0.1 -port %d"
       (Filename.concat Config.c.src_directory "dune-client")
       dir
       (rpc_port i))
    fmt

let start_node dir =
  tezos_node "run --data-dir %s &" dir ;
  let lock_file = Filename.concat dir "lock" in
  check_file lock_file true 10 ;
  started := Some dir

(* Incompatibilities:
  * Identities generated with mainnet do not contain 'peer_id' as generated
    by master.
*)

let tezos_activate_alpha dir i =
  tezos_client
    dir
    i
    {|-block genesis activate protocol %s with fitness 1 and key activator and parameters %S|}
    Config.c.proto_hash
    (Filename.concat (Filename.dirname dir) Data.protocol_parameters_file_name)

let tezos_init_wallet dir i =
  let client_dir = Filename.concat dir "client" in
  if not (Sys.file_exists client_dir) then (
    FileString.safe_mkdir client_dir ;
    List.iter
      (fun id ->
        match id.pseudo with
        | None ->
            ()
        | Some name ->
            tezos_client
              dir
              i
              {| -p %s import secret key %s unencrypted:%s |}
              Config.c.proto_hash
              name
              id.private_key)
      Config.c.identities
    (*
    tezos_client dir i {| -p %s import secret key activator unencrypted:%s |}
      Config.c.proto_hash
      Data.activator_secret;
*)
    )

let rec bootstrap_peers n =
  if n = 1 then []
  else
    let n = n - 1 in
    let peer = Printf.sprintf "127.0.0.1:%d" (p2p_port n) in
    Printf.sprintf "%S" peer :: bootstrap_peers n

let spaces = String.make 100 ' '

type state = NEED_COMMA | NEED_EOL | NEED_NOTHING

let create_node_config dir i =
  let b = Buffer.create 10000 in
  let indent = ref 0 in
  let state = ref NEED_NOTHING in
  let line fmt =
    Printf.kprintf
      (fun s ->
        ( match !state with
        | NEED_COMMA ->
            Buffer.add_string b ",\n"
        | NEED_EOL ->
            Buffer.add_string b "\n"
        | NEED_NOTHING ->
            () ) ;
        Buffer.add_substring b spaces 0 !indent ;
        Buffer.add_string b s ;
        state := NEED_EOL)
      fmt
  in
  let no_comma_here () =
    match !state with NEED_COMMA -> state := NEED_EOL | _ -> ()
  in
  let indent f =
    indent := !indent + 2 ;
    f () ;
    indent := !indent - 2
  in
  let section name f =
    line "%S: {" name ;
    indent f ;
    no_comma_here () ;
    line "}" ;
    state := NEED_COMMA
  in
  let field name fmt =
    Printf.kprintf
      (fun value ->
        line "%S: %s" name value ;
        state := NEED_COMMA)
      fmt
  in
  let string name fmt =
    Printf.kprintf
      (fun value ->
        line "%S: %S" name value ;
        state := NEED_COMMA)
      fmt
  in
  line "{" ;
  indent (fun () ->
      string "data-dir" "%s" dir ;
      section "p2p" (fun () ->
          string "listen-addr" "127.0.0.1:%d" (p2p_port i) ;
          field "private-mode" "false" ;
          field "expected-proof-of-work" "%f" Config.c.pow_power ;
          field
            "bootstrap-peers"
            "[%s]"
            (String.concat "," (bootstrap_peers i)) ;
          section "limits" (fun () ->
              field "min-connections" "2" ;
              field "expected-connections" "3" ;
              field "max-connections" "30")) ;
      section "rpc" (fun () ->
          string "listen-addr" "127.0.0.1:%d" (rpc_port i) ;
          field "cors-origin" "[ \"http://127.0.0.1:4000\" ]" ;
          field "cors-headers" "[ \"content-type\" ]") ;
      section "shell" (fun () ->
          section "chain_validator" (fun () -> field "bootstrap_threshold" "1"))) ;
  no_comma_here () ;
  line "}" ;
  line "" ;
  Buffer.contents b

let really_create_node identities_dir dir i =
  FileString.safe_mkdir dir ;
  Printf.eprintf "Directory %s created.\n%!" dir ;
  let env_sh_content =
    Printf.sprintf
      {|
  if test -z "$NODE_DIR"; then
    echo "The variable NODE_DIR must be defined."
    exit 2
fi

. $NODE_DIR/../env.sh

DUNE_NODE=node%d
DUNE_ADDR=127.0.0.1
DUNE_RPC_PORT=%d
DUNE_ACCOUNT=bootstrap%d
|}
      i
      (rpc_port i)
      i
  in
  let env_sh_file = Filename.concat dir "env.sh" in
  FileString.write_file env_sh_file env_sh_content ;
  Unix.chmod env_sh_file 0o755 ;
  List.iter
    (fun script_name ->
      let filename = Filename.concat dir script_name in
      (try Sys.remove filename with _ -> ()) ;
      Unix.symlink (Filename.concat "../shared" script_name) filename)
    [ "dune-node.sh";
      "dune-activate.sh";
      "dune-client.sh";
      "dune-baker.sh";
      "dune-endorser.sh" ] ;
  let config_content = create_node_config dir i in
  let config_file = "config.json" in
  maybe_gen_file dir config_file config_content ;
  maybe_gen_file dir Data.version_file_name Data.version_file_content ;
  let src_identity_file = Filename.concat identities_dir (identity_file i) in
  let dst_identity_file = Filename.concat dir "identity.json" in
  if Sys.file_exists src_identity_file then (
    let content = FileString.read_file src_identity_file in
    FileString.write_file dst_identity_file content ;
    Printf.eprintf "File %S copied.\n%!" dst_identity_file )
  else (
    tezos_node "identity generate %f --data-dir '%s'" Config.c.pow_power dir ;
    let content = FileString.read_file dst_identity_file in
    FileString.write_file src_identity_file content ;
    Printf.eprintf "File %S generated.\n%!" dst_identity_file ) ;
  tezos_init_wallet dir i ;
  let activated_file = Filename.concat dir "activated.witness" in
  if (not (Sys.file_exists activated_file)) && Config.c.activate_proto then (
    start_node dir ;
    tezos_activate_alpha dir i ;
    stop_node dir ;
    FileString.write_file activated_file "done" ) ;
  ()

let create_node identities_dir dir i =
  let node_dir = Filename.concat dir (node_dir i) in
  let activated_file = Filename.concat node_dir "activated.witness" in
  if (not !force_create) && Sys.file_exists activated_file then
    Printf.eprintf "Directory for node %d already exists.\n%!" i
  else really_create_node identities_dir node_dir i

let create ~srcdir ~dir n =
  maybe_gen_file
    dir
    Data.protocol_parameters_file_name
    (Data.protocol_parameters_file_content ()) ;
  maybe_gen_file
    dir
    "env.sh"
    (Printf.sprintf
       {|
DUNE_SRCDIR=%s
DUNE_RUNDIR=%s
PROTO_HASH=%s
|}
       srcdir
       dir
       Config.c.proto_hash) ;
  let shared_dir = Filename.concat dir "shared" in
  (try Unix.mkdir shared_dir 0o755 with _ -> ()) ;
  List.iter
    (fun (script_name, script_content) ->
      maybe_gen_file shared_dir script_name script_content ;
      Unix.chmod (Filename.concat shared_dir script_name) 0o755)
    [ ("dune-node.sh", Data.shared_tezos_node_sh);
      ("dune-client.sh", Data.shared_tezos_client_sh);
      ( "dune-baker.sh",
        Data.shared_tezos_baker_sh ~proto_name:Config.c.proto_name );
      ( "dune-endorser.sh",
        Data.shared_tezos_endorser_sh ~proto_name:Config.c.proto_name );
      ("dune-activate.sh", Data.shared_tezos_activate_sh) ] ;
  let identities_dir = Filename.concat dir identities_dir in
  for i = 1 to n do
    create_node identities_dir dir i
  done ;
  ()

let for_all_node_dirs dir f =
  let rec for_all_nodes dir i f =
    let node_dir = Filename.concat dir (node_dir i) in
    if Sys.file_exists node_dir then (
      f node_dir ;
      for_all_nodes dir (i + 1) f )
  in
  for_all_nodes dir 1 f

let for_all_identity dir f =
  let rec for_all_identity dir i f =
    let node_dir = Filename.concat dir (Printf.sprintf "identity%d.json" i) in
    if Sys.file_exists node_dir then (
      f node_dir ;
      for_all_identity dir (i + 1) f )
  in
  for_all_identity dir 1 f

open Ezcmd.Modules
module Arg = Ezcmd.Modules.Arg

let help_secs =
  [ `S Manpage.s_common_options;
    `P "These options are common to all commands.";
    `S "MORE HELP";
    `P "Use `$(mname) $(i,COMMAND) --help' for help on a single command.";
    `Noblank;
    `P "Use `$(mname) help patterns' for help on patch matching.";
    `Noblank;
    `P "Use `$(mname) help environment' for help on environment variables.";
    `S Manpage.s_bugs;
    `P "Check bug reports at http://bugs.example.org." ]

let () =
  let clean_all = ref false in
  let load_config_from_file = ref None in
  let set_config_options = ref [] in
  let set_config_option f s =
    set_config_options := (fun () -> f s) :: !set_config_options
  in
  let set_config_string f = Arg.String (set_config_option f) in
  let set_config_unit f = Arg.Unit (set_config_option f) in
  let set_config_int f = Arg.Int (set_config_option f) in
  let save_config_to_file = ref None in
  let common_args =
    [ ( ["config"],
        Arg.String (fun s -> load_config_from_file := Some s),
        Ezcmd.info
          ~docv:"FILE"
          "Load config from FILE, before executing other options" );
      ( ["save-config"],
        Arg.String (fun s -> save_config_to_file := Some s),
        Ezcmd.info
          ~docv:"FILE"
          "Save config to FILE, after executing all options" );
      ( ["dir"],
        set_config_string (fun dir -> Config.c.node_directory <- dir),
        Ezcmd.info ~docv:"DIR" "Populate directory $(docv)" ) ]
  in
  let init_args =
    common_args
    @ [ ( ["src"],
          set_config_string (fun dir -> Config.c.src_directory <- dir),
          Ezcmd.info ~docv:"DIR" "Find private Dune sources in $(docv)" );
        ( ["alpha"],
          set_config_unit (fun () ->
              Config.c.proto_name <- Config.protocol_alpha.protocol_name ;
              Config.c.proto_hash <- Config.protocol_alpha.protocol_hash),
          Ezcmd.info "Set protocol to the Alpha protocol instead of private" );
        ( ["mainnet"],
          set_config_unit (fun () ->
              Config.c.proto_name <- Config.protocol_mainnet.protocol_name ;
              Config.c.proto_hash <- Config.protocol_mainnet.protocol_hash),
          Ezcmd.info "Set protocol to the Mainnet protocol instead of private"
        );
        ( ["n"],
          set_config_int (fun n -> Config.c.number_of_nodes <- n),
          Ezcmd.info ~docv:"NUM" "NUmber of node configurations to generate" );
        (["f"], Arg.Set force_create, Ezcmd.info "Force update of files");
        ( ["dont-activate-proto"],
          set_config_unit (fun () -> Config.c.activate_proto <- false),
          Ezcmd.info "Do not activate protocol 1" );
        ( ["activate-proto"],
          set_config_unit (fun () -> Config.c.activate_proto <- true),
          Ezcmd.info "Activate protocol 1" ) ]
  in
  let clean_args =
    common_args @ [(["all"], Arg.Set clean_all, Ezcmd.info "Clean everything")]
  in
  let kill_args = common_args @ [] in
  let init_man =
    [ `S Manpage.s_description;
      `P "initialize a local network of Dune private nodes";
      `Blocks help_secs ]
  in
  let clean_man =
    [ `S Manpage.s_description;
      `P "clean a directory containing Dune private node configurations";
      `Blocks help_secs ]
  in
  let kill_man =
    [ `S Manpage.s_description;
      `P
        "kill nodes started using a directory containing Dune private node \
         configurations";
      `Blocks help_secs ]
  in
  let get_directory () =
    let directory =
      if Filename.is_relative Config.c.node_directory then
        Filename.concat (Unix.getcwd ()) Config.c.node_directory
      else Config.c.node_directory
    in
    FileString.safe_mkdir directory ;
    FileString.safe_mkdir (Filename.concat directory identities_dir) ;
    directory
  in
  let init_action () =
    let directory = get_directory () in
    List.iter
      (fun command ->
        let bin = Filename.concat Config.c.src_directory command in
        if not (Sys.file_exists bin) then
          abort "Could not find %s binary as %S" command bin)
      ["dune-node"; "dune-client"; "README.fork.md" (* private version *)] ;
    create
      ~dir:directory
      ~srcdir:Config.c.src_directory
      Config.c.number_of_nodes
  in
  let clean_action () =
    let directory = get_directory () in
    for_all_node_dirs directory (fun node_dir ->
        FileDir.remove_all (FileGen.of_string node_dir) ;
        Printf.eprintf "Removed dir %S.\n%!" node_dir ;
        if Sys.file_exists node_dir then
          abort "Dir %S still exists.\n%!" node_dir) ;
    List.iter
      (fun filename ->
        let filename = Filename.concat directory filename in
        if Sys.file_exists filename then (
          Sys.remove filename ;
          Printf.eprintf "Removed %S\n%!" filename ))
      [ "env.sh";
        "protocol_parameters.json";
        "shared/dune-baker.sh";
        "shared/dune-client.sh";
        "shared/dune-endorser.sh";
        "shared/dune-node.sh";
        "shared/dune-activate.sh" ] ;
    if !clean_all then
      for_all_identity (Filename.concat directory identities_dir) (fun file ->
          Printf.eprintf "Removing %S\n%!" file ;
          Sys.remove file)
  in
  let kill_action () =
    let directory = get_directory () in
    for_all_node_dirs directory (fun dir -> maybe_stop_node dir)
  in
  let action f () =
    ( match !load_config_from_file with
    | None ->
        ()
    | Some filename ->
        Printf.eprintf "Loading config from file %s\n%!" filename ;
        Config.load filename ) ;
    List.iter (fun f -> f ()) !set_config_options ;
    let cwd = Sys.getcwd () in
    if Filename.is_relative Config.c.node_directory then
      Config.c.node_directory <- Filename.concat cwd Config.c.node_directory ;
    if Filename.is_relative Config.c.src_directory then
      Config.c.src_directory <- Filename.concat cwd Config.c.src_directory ;
    ( match !save_config_to_file with
    | None ->
        ()
    | Some filename ->
        Config.save filename ;
        Printf.eprintf "Config saved. Exiting.\n%!" ;
        exit 0 ) ;
    f ()
  in
  let init_cmd =
    {
      Arg.cmd_name = "init";
      cmd_doc = "initialize a local network of Dune private nodes";
      cmd_args = init_args;
      cmd_action = action init_action;
      cmd_man = init_man;
    }
  in
  let clean_cmd =
    {
      Arg.cmd_name = "clean";
      cmd_doc = "clean a directory containing Dune private node configurations";
      cmd_args = clean_args;
      cmd_action = action clean_action;
      cmd_man = clean_man;
    }
  in
  let kill_cmd =
    {
      Arg.cmd_name = "kill";
      cmd_doc =
        "kill nodes started from a directory containing Dune private node \
         configurations";
      cmd_args = kill_args;
      cmd_action = action kill_action;
      cmd_man = kill_man;
    }
  in
  Ezcmd.main_with_subcommands
    ~name:"dune-gen"
    ~version:"0.1"
    ~default:"init"
    ~doc:"A private Dune network configuration tool"
    ~man:([`P "Hello"] @ help_secs)
    [init_cmd; clean_cmd; kill_cmd]
