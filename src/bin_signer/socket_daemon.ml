(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Signer_logging
open Signer_messages

let log = lwt_log_notice

let handle_client ?(close_fd = true) ?magic_bytes ~check_high_watermark
    ~require_auth cctxt fd =
  let close () = if close_fd then Lwt_unix.close fd else Lwt.return_unit in
  Lwt_utils_unix.Socket.recv fd Request.encoding
  >>=? function
  | Sign req ->
      let encoding = result_encoding Sign.Response.encoding in
      Handler.sign cctxt req ?magic_bytes ~check_high_watermark ~require_auth
      >>= fun res ->
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit
  | Deterministic_nonce req ->
      let encoding = result_encoding Deterministic_nonce.Response.encoding in
      Handler.deterministic_nonce cctxt req ~require_auth
      >>= fun res ->
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit
  | Deterministic_nonce_hash req ->
      let encoding =
        result_encoding Deterministic_nonce_hash.Response.encoding
      in
      Handler.deterministic_nonce_hash cctxt req ~require_auth
      >>= fun res ->
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit
  | Supports_deterministic_nonces req ->
      let encoding =
        result_encoding Supports_deterministic_nonces.Response.encoding
      in
      Handler.supports_deterministic_nonces cctxt req
      >>= fun res ->
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit
  | Public_key pkh ->
      let encoding = result_encoding Public_key.Response.encoding in
      Handler.public_key cctxt pkh
      >>= fun res ->
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit
  | Authorized_keys ->
      let encoding = result_encoding Authorized_keys.Response.encoding in
      ( if require_auth then
        Handler.Authorized_key.load cctxt
        >>=? fun keys ->
        return
          (Authorized_keys.Response.Authorized_keys
             (keys |> List.split |> snd |> List.map Signature.Public_key.hash))
      else return Authorized_keys.Response.No_authentication )
      >>= fun res ->
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit

let rec retry ?(max_attempt = 5) cpt f =
  ( if cpt <> 0 then
    log
      Tag.DSL.(
        fun f ->
          f "retry number %a" -% t event "sign_retry" -% a port_number cpt)
  else Lwt.return_unit )
  >>= fun () ->
  f ()
  >>= fun res ->
  match res with
  | Ok _ ->
      Lwt.return res
  | Error errs ->
      List.iter (Error_monad.pp Format.err_formatter) errs ;
      if cpt >= max_attempt then
        log
          Tag.DSL.(
            fun f ->
              f "retry max attempt number reached %a"
              -% t event "sign_max_attempt" -% a port_number cpt)
        >>= fun () -> Lwt.return res
      else Lwt_unix.sleep 2. >>= fun () -> retry ~max_attempt (cpt + 1) f

let handle_client_protected ?(close_fd = true) ?magic_bytes
    ~check_high_watermark ~require_auth cctxt fd =
  let close () = if close_fd then Lwt_unix.close fd else Lwt.return_unit in
  Lwt_utils_unix.Socket.recv fd Request.encoding
  >>=? fun req ->
  (* handle_client req *)
  log Tag.DSL.(fun f -> f "[%a]" -% t event "bounce_req" -% a req_type req)
  >>= fun () ->
  match req with
  | Sign req ->
      let f () =
        Handler.sign cctxt req ?magic_bytes ~check_high_watermark ~require_auth
      in
      retry 0 f
      >>= fun res ->
      let encoding = result_encoding Sign.Response.encoding in
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit
  | Deterministic_nonce req ->
      let f () = Handler.deterministic_nonce cctxt req ~require_auth in
      retry 0 f
      >>= fun res ->
      let encoding = result_encoding Deterministic_nonce.Response.encoding in
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit
  | Deterministic_nonce_hash req ->
      let f () = Handler.deterministic_nonce_hash cctxt req ~require_auth in
      retry 0 f
      >>= fun res ->
      let encoding =
        result_encoding Deterministic_nonce_hash.Response.encoding
      in
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit
  | Supports_deterministic_nonces req ->
      let f () = Handler.supports_deterministic_nonces cctxt req in
      retry 0 f
      >>= fun res ->
      let encoding =
        result_encoding Supports_deterministic_nonces.Response.encoding
      in
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit
  | Public_key pkh ->
      let f () = Handler.public_key cctxt pkh in
      retry 0 f
      >>= fun res ->
      let encoding = result_encoding Public_key.Response.encoding in
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit
  | Authorized_keys ->
      let encoding = result_encoding Authorized_keys.Response.encoding in
      ( if require_auth then
        Handler.Authorized_key.load cctxt
        >>=? fun keys ->
        return
          (Authorized_keys.Response.Authorized_keys
             (keys |> List.split |> snd |> List.map Signature.Public_key.hash))
      else return Authorized_keys.Response.No_authentication )
      >>= fun res ->
      Lwt_utils_unix.Socket.send fd encoding res
      >>= fun _ -> close () >>= fun () -> return_unit

let run (cctxt : #Client_context.wallet) path ?magic_bytes
    ~check_high_watermark ~require_auth =
  let open Lwt_utils_unix.Socket in
  ( match path with
  | Tcp (host, service, _opts) ->
      log
        Tag.DSL.(
          fun f ->
            f "Accepting TCP requests on %s:%s"
            -% t event "accepting_tcp_requests"
            -% s host_name host -% s service_name service)
  | Unix path ->
      ListLabels.iter
        Sys.[sigint; sigterm]
        ~f:(fun signal ->
          Sys.set_signal
            signal
            (Signal_handle
               (fun _ ->
                 Format.printf "Removing the local socket file and quitting.@." ;
                 Unix.unlink path ;
                 exit 0))) ;
      log
        Tag.DSL.(
          fun f ->
            f "Accepting UNIX requests on %s"
            -% t event "accepting_unix_requests"
            -% s unix_socket_path path) )
  >>= fun () ->
  bind path
  >>=? fun fds ->
  let rec loop fd =
    Lwt_unix.accept fd
    >>= fun (cfd, _) ->
    Lwt.async (fun () ->
        protect
          ~on_error:(function
            | Exn End_of_file :: _ ->
                return_unit
            | errs ->
                Lwt.return_error errs)
          (fun () ->
            handle_client
              ?magic_bytes
              ~check_high_watermark
              ~require_auth
              cctxt
              cfd)) ;
    loop fd
  in
  Lwt_list.iter_p loop fds >>= return

let run_reverse (cctxt : #Client_context.wallet) path ?magic_bytes
    ~check_high_watermark ~require_auth =
  let open Lwt_utils_unix.Socket in
  ( match path with
  | Tcp (host, service, _opts) ->
      log
        Tag.DSL.(
          fun f ->
            f "Sending TCP requests on %s:%s"
            -% t event "accepting_tcp_requests"
            -% s host_name host -% s service_name service)
  | Unix _ ->
      Lwt.fail_with "Unix socket not implemented yet" )
  >>= fun () ->
  connect path
  >>=? fun fd ->
  Lwt_unix.setsockopt fd Lwt_unix.SO_KEEPALIVE true ;
  let rec loop () =
    handle_client_protected
      ~close_fd:false
      ?magic_bytes
      ~check_high_watermark
      ~require_auth
      cctxt
      fd
    >>=? loop
  in
  protect
    ~on_error:(function
      | [Exn End_of_file] ->
          Lwt_unix.close fd >>= return
      | errs ->
          Lwt_unix.close fd >>= fun () -> Lwt.return_error errs)
    loop

let run_proxy path =
  let open Lwt_utils_unix.Socket in
  ( match path with
  | Tcp (host, service, _opts) ->
      log
        Tag.DSL.(
          fun f ->
            f "Accepting TCP proxy requests on %s:%s"
            -% t event "accepting_tcp_proxy_requests"
            -% s host_name host -% s service_name service)
  | Unix _ ->
      assert false )
  >>= fun () ->
  bind path
  >>=? fun fds ->
  let rec proxy_loop fd =
    Lwt_unix.accept fd
    >>= fun (cfd, _) ->
    Lwt.async (fun () ->
        protect
          ~on_error:(function
            | [Exn End_of_file] -> return_unit | errs -> Lwt.return_error errs)
          (fun () ->
            ( match Lwt_unix.getpeername cfd with
            | ADDR_UNIX _ ->
                assert false
            | ADDR_INET (host, service) ->
                log
                  Tag.DSL.(
                    fun f ->
                      f "receiving proxy request from %s:%s"
                      -% t event "receiving tcp proxy request"
                      -% s host_name (Unix.string_of_inet_addr host)
                      -% s service_name (string_of_int service)) )
            >>= fun () -> Tezos_signer_backends_unix.Proxy.init_connection cfd)) ;
    proxy_loop fd
  in
  return @@ Lwt.async (fun () -> Lwt_list.iter_p proxy_loop fds)
